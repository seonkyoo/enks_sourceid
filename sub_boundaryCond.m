%% -------- inlet & outlet conditions -------- 
function [Uin, dP, Uthrgh] = sub_boundaryCond(M)
    input_physicalConst
    input_gridStructure
    
    % pressure boundary
    Uin = 0;
    if M ~= 0
        dP = DelRho*g*Lx/M;
    elseif M == 0
        dP = DelRho*9.8*Lx/0.001;
    end

    Uthrgh = kTrueMean/visco/phi*dP/Lx;
return
