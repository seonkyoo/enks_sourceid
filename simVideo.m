%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           simVideo.m                                 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
%  Developed by:        
%       Seonkyoo Yoon < yoonx213@umn.edu >                     
% 
%  DESCRIPTION:   
%       generate videos of plume migration
% 
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clc; clf; % gcp

addpath('/home/pkkang/yoonx213/graphTools/MRST/'); 
addpath('/home/pkkang/yoonx213/graphTools/MRST/mrst-2014b'); startup; 

codeDir = '~/sourceID/EnKS_sourceID/';
addpath(codeDir);

varioType = 'Sph3d';
itrue = 527;
kVar = 0.5;
nSrc = 3;
nSrcInit = 3;
doClstr = 1;

kMatDir = [codeDir sprintf(['%s/'],varioType')];



%% ------------------------------------------------------------------------
% General Settings
%--------------------------------------------------------------------------
input_gridStructure       % Grid structure
M = 0; input_physicalConst

if exist([kMatDir 'Grid.mat'],'file')
    load([kMatDir 'Grid.mat'],'Grid');
else
    cartDims = [nx, nz, ny];       % [nx,ny,nz]
    physDims = cartDims.*[dx,dz,dy]; % [dx,dy,dz]

    Grid = cartGrid(cartDims, physDims);
    Grid = computeGeometry(Grid);
    save([kMatDir 'Grid.mat'],'Grid');
end


%% K-Field
if ny > 1
    load([kMatDir '3DKmat_var1.mat'],'Kmat')
    Kmat = reshape(Kmat,100,100,100,27);
    
    nEns = 0;
    for ie = 1:27
        for iy = 1:100/ny
            for ix = 1:100/nx
                for iz = 1:100/nz
                    nEns = nEns + 1;
                    tmp = Kmat((iz-1)*nz+1:iz*nz,(ix-1)*nx+1:ix*nx,(iy-1)*ny+1:iy*ny,ie);
                    kMat(:,nEns) = tmp(:);
                end
            end
        end
    end
%     Kmat = Kmat(1:nz,1:nx,1:ny);
%     Kmat = Kmat(1:nz,1:nx,1:ny);
% 
%     kTrue = Kmat(:);
else
    load([kMatDir 'Kmat_var1.mat'],'Kmat')
    Kmat = reshape(Kmat,50,500,301);
    for iEns = 1:301
        for iFrme = 1:5
            tmp  = Kmat(:,(iFrme-1)*100+1:iFrme*nx,iEns);
            kMat(:,(iEns-1)*5+iFrme) = tmp(:);
        end
    end
end
clear Kmat
kTrue = kMat(:,itrue);

kTrue = exp(log(kTrue)/mean(log(kTrue))*log(kTrueMean));
kTrue = exp((log(kTrue)-log(kTrueMean))/std(log(kTrue))*sqrt(kVar)+log(kTrueMean));

%%
clc
clf
args = {'FaceAlpha'; 0.9; 'EdgeAlpha'; 0.001; 'EdgeColor'; 'k'};

% h = plotCellData(Grid, MRSTindexing(kTrue,Grid), args{:}); 

% plotGrid(Grid, 'FaceColor', 'none', 'EdgeAlpha', 0.05); hold on

indx = ones(nz,nx,ny);
% indx([1:round(nz)],[round(nx/2):nx],[1:round(ny/2)]) = 0;
% indx([1:round(nz/2)],[round(nx/nx):nx],[1:round(ny/2)]) = 0;
% indx([1:round(nz/2)],[round(nx/2):nx],[round(ny/2):round(ny)]) = 0;
    
h = plotCellData(Grid, MRSTindexing(log(kTrue),Grid),...
        MRSTindexing(indx(:),Grid) == 1, args{:}); 

colorbar;
    xlabel('x','fontsize',25)
    ylabel('z','fontsize',25)
    zlabel('y','fontsize',25)

%     view(20,30)
    view(10,-50)
    set(gca,'zdir','normal')
% view(3)
    axis equal tight %off
%     suptitle(sprintf('M = %.3f  t = %.2f [PVI] \n',M,it/nObsT));
    hold off;
%% true parameter (X) & observation (Y) generation

Xtrue = nan(4*nSrc,1); % [srcLoc, srcCnc, k]
Xtrue(1:nSrc*3,1) = [11; 17; 5; ...    xSrc1; ySrc1; zSrc1;
                     4; 9; 12; ...    xSrc2; ySrc2; zSrc2;
                     4; 17; 20];%      xSrc3; ySrc3; zSrc3;
Xtrue(3*nSrc+1:end,1) = [1; 1; 1; ];



%%

xRange = nan(length(Xtrue),2); 

% range: srcLoc 
for iSrc = 1:nSrcInit
    xRange(iSrc*3-2:iSrc*3,:) = [2 14; 7 19; 2 23];
end

symb = '^*d';

clc
xSrcTrue = Xtrue([1:3:3*nSrcInit-2]);
ySrcTrue = Xtrue([2:3:3*nSrcInit-1]);
zSrcTrue = Xtrue([3:3:3*nSrcInit]);

obsType = ['cnc';'prf']'; 
[~, obsLoc, ~, ~] = sub_obsSetting(M,obsType,10,5,4,5);
clf
    plotGrid(Grid, 'FaceColor', 'none', 'EdgeAlpha', 0.05); hold on
   
    scatter3(obsLoc(:,1),obsLoc(:,3),obsLoc(:,2),10,'filled'); hold on
    
for iSrc = 1:nSrc
    scatter3(xSrcTrue(iSrc),zSrcTrue(iSrc),ySrcTrue(iSrc),50,'r',symb(iSrc),'linewidth',2); 
end

plot3(xRange(1,:),xRange(3,[1 1]),xRange(2,[1 1]),'k')
plot3(xRange(1,:),xRange(3,[end end]),xRange(2,[1 1]),'k')
plot3(xRange(1,[1 1]),xRange(3,[1 end]),xRange(2,[1 1]),'k')
plot3(xRange(1,[end end]),xRange(3,[1 end]),xRange(2,[1 1]),'k')
plot3(xRange(1,[end end]),xRange(3,[1 1]),xRange(2,:),'k')
plot3(xRange(1,[1 1]),xRange(3,[1 1]),xRange(2,:),'k')
plot3(xRange(1,:),xRange(3,[1 1]),xRange(2,[end end]),'k')
plot3(xRange(1,[end end]),xRange(3,:),xRange(2,[end end]),'k')
plot3(xRange(1,[1 1]),xRange(3,:),xRange(2,[end end]),'k--')
plot3(xRange(1,:),xRange(3,[end end]),xRange(2,[end end]),'k--')
plot3(xRange(1,[1 1]),xRange(3,[end end]),xRange(2,[1 end]),'k--')
plot3(xRange(1,[end end]),xRange(3,[end end]),xRange(2,[1 end]),'k')

xlabel('x')
ylabel('z')
zlabel('y')

set(gca','zdir','normal')
    indx = zeros(nz,nx,ny);
%     indx(cmat(:,it)>0.01) = 1;
        view(10,-50)
        axis equal tight %off
%   
%     indx([1:round(nz/2)],[1:nx],[1:round(ny/2)]) = 0;
    
%     h = plotCellData(Grid, MRSTindexing(cmat(:,it),Grid),...
%         MRSTindexing(indx(:),Grid) == 1, args{:}); 


videoDir = [codeDir 'video/'];
figName = [videoDir sprintf('grid_obsLoc.png')];
print(figName,'-dpng');
 
%% ------------------------------------------------------------------------
% simulation
%--------------------------------------------------------------------------
%clc
%
%for M = [0.1  1  10]%M = 0;
%dataDir = [codeDir sprintf(['%s/itrue%d/kVar%.2f/M%.3f/'],varioType',itrue,kVar,M)];
%
%%input_physicalConst
%
%[~, ~, Uthrgh] = sub_boundaryCond(M)
%%Uin = 0;
%%if M ~= 0
%%    dP = DelRho*g*Lx/M;
%%elseif M == 0
%%    dP = DelRho*9.8*Lx/0.001;
%%end
%%Uthrgh = kTrueMean/visco/phi*dP/Lx;
%c0 = zeros(nz,nx,ny);  
%
%t = 0;
%tf = Lx / Uthrgh;                   % 1 PVI [sec]
%nObsT = 50;
%dt = tf/nObsT;
%
%flName = [dataDir sprintf('simResults_dt%.2fPVI.mat',dt/tf)];
%
%if ~exist(flName,'file')
%pmat = nan(nz*nx*ny,nObsT);
%cmat = nan(nz*nx*ny,nObsT);
%
%for it = 1:nObsT
%    [pmat(:,it), cmat(:,it)]=sub_forwardModel_dt(Xtrue(1:3*nSrc),Xtrue(3*nSrc+1:end),kTrue,...
%                                                           c0,t, t+dt, M);
%    t = t+dt;
%    c0 = reshape(cmat(:,it),nz,nx,ny);
%
%    fprintf('M = %.3f  t = %.2fPVI \n',M,t/tf);
%
%end
%
%save([dataDir sprintf('simResults_dt%.2fPVI.mat',dt/tf)],'pmat','cmat');
%end
%end

%% ------------------------------------------------------------------------
% sweeping efficiency analysis
%--------------------------------------------------------------------------
%clc; clf;
%
%videoDir = [codeDir 'video/'];
%figName = [videoDir sprintf('sweepEff_seed%d_dt%.2fPVI.png',itrue,1/nObsT)];
%
%sweepEffAll = cell(3,1);
%iM = 0;
%for M = [0.1  1  10]
%iM = iM + 1;
%dataDir = [codeDir sprintf(['%s/itrue%d/kVar%.2f/M%.3f/'],varioType',itrue,kVar,M)];
%load([dataDir sprintf('simResults_dt%.2fPVI.mat',1/nObsT)],'pmat','cmat');
%
%flName = [dataDir sprintf('sweepEff_dt%.2fPVI.mat',dt/tf)];
%
%
%if exist(flName,'file')
%    load(flName,'sweepEff')
%else
%    sweepEff = zeros(nObsT,2);
%    for it = 1:nObsT
%        fprintf('sweeping eff. M = %.3f  t = %.2fPVI \n',M,it/nObsT);
%        
%        indx = zeros(nz,nx,ny);
%        sweepEff(it,1) = sum(cmat(:,it)>0.00)/(nz*nx*ny);
%        sweepEff(it,2) = sum(cmat(:,it)>0.01)/(nz*nx*ny);
%        
%    end
%    save(flName,'sweepEff')
%end    
%
%sweepEffAll{iM} = sweepEff;
%end
%
%subplot(211)
%for iM = 1:3
%    plot([1:nObsT]'/nObsT,sweepEffAll{iM}(:,1)); hold on
%end
%legend('M0.1', 'M1','M10')
%xlabel('time [pv]')
%ylabel('sweeping Efficiency')
%title('c>0')
%
%subplot(212)
%for iM = 1:3
%    plot([1:nObsT]'/nObsT,sweepEffAll{iM}(:,2)); hold on
%end
%legend('M0.1', 'M1','M10','location','northwest')
%xlabel('time [pv]')
%ylabel('sweeping Efficiency')
%title('c>0.01')
%
%print(figName,'-dpng');



%%% ------------------------------------------------------------------------
%% figures
%%--------------------------------------------------------------------------
%clc
%
%args = {'FaceAlpha'; 0.3; 'EdgeAlpha'; 0.001; 'EdgeColor'; 'k'};
%
%
%for M = [0.1  1  10]
%% close all; figure
%clf
%set(gcf,'color','w')
%
%dataDir = [codeDir sprintf(['%s/itrue%d/kVar%.2f/M%.3f/'],varioType',itrue,kVar,M)];
%
%load([dataDir sprintf('simResults_dt%.2fPVI.mat',1/nObsT)],'pmat','cmat');
%
%for it = 1:nObsT
%    fprintf('Figure M = %.3f  t = %.2fPVI \n',M,it/nObsT);
%    clf
%    
%    plotGrid(Grid, 'FaceColor', 'none', 'EdgeAlpha', 0.05); hold on
%    indx = zeros(nz,nx,ny);
%    indx(cmat(:,it)>0.01) = 1;
%%     indx([1:round(nz/2)],[1:nx],[1:round(ny/2)]) = 0;
%    
%    h = plotCellData(Grid, MRSTindexing(cmat(:,it),Grid),...
%        MRSTindexing(indx(:),Grid) == 1, args{:}); 
%
%    xlabel('x','fontsize',25)
%    ylabel('y','fontsize',25)
%    zlabel('z','fontsize',25)
%
%    view(20,30)
%% view(3)
%    axis equal tight %off
%    suptitle(sprintf('M = %.3f  t = %.2f [PVI] \n',M,it/nObsT));
%    hold off;
%%     colorbar; 
%    caxis([0 1])
%
%%     drawnow;
%
%    fig = gcf;
%    fig.Units = 'inches';
%    fig.PaperPosition = [0 0 10 5];
%  
%%     saveas(gcf,[dataDir sprintf('simFig%.2fPVI.png',it/nObsT)],'png');
%    print([dataDir sprintf('simFig%.2fPVI.png',it/nObsT)],'-dpng');
%end
%    
%end
%
%%% ------------------------------------------------------------------------
%% videos
%%--------------------------------------------------------------------------
%videoDir = [codeDir 'video/'];
%mkdir(videoDir)
%for M = [ 0.1  1  10]%M = 0;
%    dataDir = [codeDir sprintf(['%s/itrue%d/kVar%.2f/M%.3f/'],varioType',itrue,kVar,M)];
%
%    fprintf('Video M = %.3f  \n',M);
%
%    videoName = [videoDir sprintf('seed%d_M%.3f_dt%.2fPVI.avi',itrue,M,1/nObsT)];
%    outputVideo = VideoWriter(videoName);
%    outputVideo.FrameRate = 5;
%    open(outputVideo)
%    for it = 1:nObsT
%        figName = [dataDir sprintf('simFig%.2fPVI.png',it/nObsT)];
%        img = imread(figName);
%        writeVideo(outputVideo,img);
%    end
%    close(outputVideo);
%
%end
