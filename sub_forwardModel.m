%% %%%%%%%%%%%%%%% forward simulation %%%%%%%%%%%%%%% 
function [Y] = sub_forwardModel(srcLoc, srcHist, kVec,obsDataFlName)
%{
    X: ensemble vector [x,z,source scenario, KL terms]
    kVec: permeability fields
%}

caseRecog
input_gridStructure         % Grid structure
input_physicalConst     % physical constants 
if M == 0; g = 0; end

flName = [dataDir obsDataFlName '.mat'];
flNameOcc1 = [dataDir obsDataFlName '.occ'];
flNameOcc2 = [caseDir obsDataFlName '.occ'];
fidocc = fopen(flNameOcc1, 'a'); fclose(fidocc);    
fidocc = fopen(flNameOcc2, 'a'); fclose(fidocc);    


%% -------- source & obs. locations ----------
xSource = srcLoc(1:3:end-2);
ySource = srcLoc(2:3:end-1);
zSource = srcLoc(3:3:end);

[~,ixSrc] = min(abs(xCells-xSource'));
[~,iySrc] = min(abs(yCells-ySource'));
[~,izSrc] = min(abs(zCells-zSource'));

indSrc = (iySrc'-1)*nz*nx + (ixSrc'-1)*nz + izSrc';

% srcHist = reshape(srcHist,numel(tObs),numel(indSrc));

[tObs, obsLoc, ~, ~] = sub_obsSetting(M,obsType,nObsTime,nxObs,nyObs,nzObs);

nObsLoc = length(obsLoc);
%% transmissibility matrix
kMat = reshape(kVec,nz,nx,ny);
[T,Lmat,invDmat,perm] = sub_transMat(M,kMat);
% transMat

%% Flow and transport 
[Uin, dP, ~] = sub_boundaryCond(M); % inlet & outlet contions

iterFig = 0;
iFigName = 0;
it = 1; doRecord = false;
c0 = zeros(nz,nx,ny);  

t = 0;
while abs(tObs(end) - t) >0.000001 
    c0(indSrc) = srcHist;%(it,:);
    %%  pressure field
    if g ~= 0 || ~exist('pmat','var')
        c0_avg = cat(1, zeros(1,nx,ny), (c0(1:end-1,:,:)+c0(2:end,:,:))/2, zeros(1,nx,ny));

        b = + T.U.*(rho0+DelRho*c0_avg(1:end-1,:,:))*g*dz ... vertical flux
            - T.D.*(rho0+DelRho*c0_avg(2:end,:,:))*g*dz;

        Phydro = repmat(rho0*g*dz*[1:nz]',1,ny); % hydrostatic pressure at the right boundary
        Phydro = reshape(Phydro, nz,1,ny);

        if Uin ~= 0 && dP == 0
            b(:,1,:) = b(:,1,:) + Uin*dz*dy  ;
        elseif Uin == 0 && dP ~= 0
            b(:,1,:) = b(:,1,:) + T.L(:,1,:).* (Phydro+dP);
        end
        b(:,end,:) = b(:,end,:) + T.R(:,end,:).* (Phydro);

        bvec = b(:);
        
        pvec(perm,:)=Lmat'\(invDmat.*(Lmat\(bvec(perm,:))));

        pmat=reshape(pvec,nz,nx,ny);
        
    end
        
    %% Transport (advection)
    Pzdif=[pmat(2:end,:,:)-pmat(1:end-1,:,:)]-(rho0+DelRho*c0_avg(2:end-1,:,:))*g*dz;
    Pxdif=[pmat(:,2:end,:)-pmat(:,1:end-1,:)];
    Pydif=[pmat(:,:,2:end)-pmat(:,:,1:end-1)];

    if Uin ~= 0 % if flux inlet B.C.
        Pxdif_L = cat(2, zeros(nz,1,ny), Pxdif);        % flux inlet B.C.
    else
        Pxdif_L = cat(2, pmat(:,1,:)-(Phydro+dP), Pxdif);% pressure inlet B.C.
    end
    Pxdif_R = cat(2, Pxdif, Phydro-pmat(:,end,:)); % pressure outlet B.C.
    Pzdif_U = cat(1, zeros(1,nx,ny), Pzdif);
    Pzdif_D = cat(1, Pzdif, zeros(1,nx,ny));
    Pydif_F = cat(3, zeros(nz,nx,1), Pydif);
    Pydif_B = cat(3, Pydif, zeros(nz,nx,1));

    U_L = -T.L/dz/dy.*Pxdif_L;     
    if Uin ~= 0 % if flux inlet B.C.
        U_L(:,1) = Uin;
    end
    U_R = -T.R/dz/dy.*Pxdif_R;
    U_U = -T.U/dx/dy.*Pzdif_U;
    U_D = -T.D/dx/dy.*Pzdif_D;
    U_F = -T.F/dz/dx.*Pydif_F;
    U_B = -T.B/dz/dx.*Pydif_B;

    c0_L = cat(2, zeros(nz,1,ny), c0(:,1:end-1,:));  
    c0_R = cat(2, c0(:,2:end,:), zeros(nz,1,ny));    
    c0_U = cat(1, zeros(1,nx,ny), c0(1:end-1,:,:));       
    c0_D = cat(1, c0(2:end,:,:), zeros(1,nx,ny));
    c0_F = cat(3, zeros(nz,nx,1), c0(:,:,1:end-1));
    c0_B = cat(3, c0(:,:,2:end), zeros(nz,nx,1));

    e0=zeros(nz,nx,ny); 
    Fadv_L = e0; Fadv_R = e0; 
    Fadv_D = e0; Fadv_U = e0; 
    Fadv_F = e0; Fadv_B = e0; 

    Fadv_L(U_L>=0) = U_L(U_L>=0) .* c0_L(U_L>=0)*dz*dy; 
    Fadv_L(U_L<0)  = U_L(U_L<0)  .* c0(U_L<0)   *dz*dy; 
    Fadv_R(U_R>=0) = U_R(U_R>=0) .* c0(U_R>=0)  *dz*dy;
    Fadv_R(U_R<0)  = U_R(U_R<0)  .* c0_R(U_R<0) *dz*dy;
    Fadv_U(U_U>=0) = U_U(U_U>=0) .* c0_U(U_U>=0)*dx*dy; 
    Fadv_U(U_U<0)  = U_U(U_U<0)  .* c0(U_U<0)   *dx*dy; 
    Fadv_D(U_D>=0) = U_D(U_D>=0) .* c0(U_D>=0)  *dx*dy;
    Fadv_D(U_D<0)  = U_D(U_D<0)  .* c0_D(U_D<0) *dx*dy;
    Fadv_F(U_F>=0) = U_F(U_F>=0) .* c0_F(U_F>=0)*dz*dx;
    Fadv_F(U_F<0)  = U_F(U_F<0)  .* c0(U_F<0)   *dz*dx;
    Fadv_B(U_B>=0) = U_B(U_B>=0) .* c0(U_B>=0)  *dz*dx;
    Fadv_B(U_B<0)  = U_B(U_B<0)  .* c0_B(U_B<0) *dz*dx;

    Fadv = (-Fadv_F -Fadv_L - Fadv_U + Fadv_D + Fadv_R + Fadv_B);    

    %% Transport (Diffusion)
    Fdif_L = (10^-9+D*abs(U_L)).*(c0 - c0_L)/dx*dz*dy;
    Fdif_R = (10^-9+D*abs(U_R)).*(c0_R - c0)/dx*dz*dy;
    Fdif_U = (10^-9+D*abs(U_U)).*(c0 - c0_U)/dz*dx*dy;
    Fdif_D = (10^-9+D*abs(U_D)).*(c0_D - c0)/dz*dx*dy; 
    Fdif_F = (10^-9+D*abs(U_F)).*(c0 - c0_F)/dy*dz*dx; 
    Fdif_B = (10^-9+D*abs(U_B)).*(c0_B - c0)/dy*dz*dx; 

    Fdif = (-Fdif_F -Fdif_L - Fdif_U + Fdif_D + Fdif_R + Fdif_B); 

    %% Advance in Time
    umax = max([...
        max(max(max(abs(cat(3, U_F, U_B(:,:,end)))))), ...
        max(max(max(abs(cat(2, U_L, U_R(:,end,:)))))), ...
        max(max(max(abs(cat(1, U_U, U_D(end,:,:))))))  ...
        ]);
    dt_adv=.1*dy/umax;
    
    Dmax = max([...
        max(max(max(10^-9+D*abs(cat(3, U_F, U_B(:,:,end)))))), ...
        max(max(max(10^-9+D*abs(cat(2, U_L, U_R(:,end,:)))))), ...
        max(max(max(10^-9+D*abs(cat(1, U_U, U_D(end,:,:))))))  ...
        ]);
    dt_diff=.1*dx*dx/Dmax; % diffusive time scale

    dt=min(dt_adv,dt_diff);

    if t+dt >= tObs(it)
        dt = tObs(it) - t;
        doRecord = true;
    end

    t=t+dt;
    c0 = c0 - dt/(dx*dz*dy)*(Fadv-Fdif);

    if doRecord 
        % obtain the concentration outputs at specific locations and times
        if ny == 1
            obsv = interp2(xCells,zCells,c0,obsLoc(:,1),obsLoc(:,3),'spline');
        else
            obsv = interp3(xxCells,zzCells,yyCells,c0,obsLoc(:,1),obsLoc(:,3),obsLoc(:,2),'spline');
        end
        
        Y((it-1)*nObsLoc+1:it*nObsLoc) = obsv;
        
        % obtain the pressure outputs at specific locations
        if M ~= 0
            if ny == 1
                obsv = interp2(xCells,zCells,pmat,obsLoc(:,1),obsLoc(:,3),'spline');
            else
                obsv = interp3(xxCells,zzCells,yyCells,pmat,obsLoc(:,1),obsLoc(:,3),obsLoc(:,2),'spline');
            end
            Y((it-1)*nObsLoc+1+nObsTime*nObsLoc:it*nObsLoc+nObsTime*nObsLoc) = obsv;
        end

        it = it +1;
        
        doRecord = false;
    end
    
end
if M == 0 % obtain the steady field of pressure outputs 
    if ny == 1
        obsv = interp2(xCells,zCells,pmat,obsLoc(:,1),obsLoc(:,3),'spline');
    else
        obsv = interp3(xxCells,zzCells,yyCells,pmat,obsLoc(:,1),obsLoc(:,3),obsLoc(:,2),'spline');
    end
    Y((it-1)*nObsLoc+1:it*nObsLoc) = obsv;
end

if numel(sd) == 1
    if strcmp(obsType','cnc')
        Y = Y(1:nObsTime*nObsLoc);
    elseif strcmp(obsType','prf')
        Y = Y(nObsTime*nObsLoc+1:end);
    end
end

Y = Y(:);

if ~exist(flName,'file'); save(flName,'Y'); end
if exist(flNameOcc1,'file'); delete(flNameOcc1); end
if exist(flNameOcc2,'file'); delete(flNameOcc2); end

return

