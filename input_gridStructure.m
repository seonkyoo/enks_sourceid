%% =========================== Grid structure =============================
%{
    GEOMETRIC INDEXING:                                                           
       The indice are arranged like this:                             

                   IndU                   +y (j)                             
                     | IndB               /                               
                     |/                  /                                
            IndL -- Ind -- IndR         /---- +x (i)                        
                    /|                  |                                 
                IndF |                  |                                 
                   IndD                +z (k)                              

       Priority: z > x > y   

       Example:                                                      
                        ____19 22 25                                    
                 ____10 13 16 |23 26                                    
               1  4  7 |14 17 |24 27                                    
               2  5  8 |15 18 |                                         
               3  6  9 |                                                
%}
%% ======================================================================== 
dx = 1; dy = 1; dz = 1; %[m]
% if strcmp(varioType(end-1:end),'3d')
    nx = 50; ny = 25; nz = 25;   % 3d
% else
%     nx = 100; ny = 1; nz = 50;   % 2d
% end


Lx = nx*dx; Ly = ny*dy; Lz = nz*dz;

nc = nz*nx*ny;  % grid cells #

xCells = [dx/2:dx:Lx]';
yCells = [dy/2:dy:Ly]';
zCells = [dz/2:dz:Lz]';

[xxCells,zzCells,yyCells]=meshgrid(xCells,zCells,yCells);