clear all; clc; clf; % gcp

input_gridStructure     % Grid structure

codeDir = '/panfs/roc/groups/6/pkkang/yoonx213/sourceID/EnKS_sourceID/';

%% --------------------------------------------------------------------
% settings
%----------------------------------------------------------------------

varioType = 'Sph3d';
for kVar = 0.5%[0.25 0.5 1 2 3]%:5
for itrue = 527%[502 527 537 563 583 603 709 721 727 851]%503 504 ]%727 851 ] %max 2*4*4*27 = 864
for M = [ 0.1  1 10]%

nSrc = 3;
nSrcInit = 3;
clstr = 1;

Ne = 500;
% txtObs=sprintf(['prf%d'], 10); figTxt = 'P';
% txtObs=sprintf(['cnc%.3f'], 0.02); figTxt = 'C';
txtObs=sprintf(['cnc%.3f_prf%d'], 0.02,10); figTxt = 'C & P';



for sLocRange = 25%[10 15 20]
for sCncRange = sLocRange 
for kMapRange = 25
inflation = 0.0;

nObsTime = 10; nzObs = 5; nxObs = 5; nyObs = 4; 
% nObsTime = 10; nzObs = 5; nxObs = 10; nyObs = 5; 

%% --------------------------------------------------------------------
% images
%----------------------------------------------------------------------

fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 10 7];

dataDir = [codeDir ...
        sprintf(['%s/itrue%d/kVar%.2f/M%.3f/nSrc%d_nSrcInit%d_clstr%d/'...
                 'Ne%d_%s/sLocRange%d_sCncRange%d_kMapRange%d_inflation%.2f/'...
                 'nObsTime%d_nxObs%d_nyObs%d_nzObs%d/'],...
                  varioType,itrue,kVar,M,nSrc,nSrcInit,clstr,...
                  Ne,txtObs,sLocRange,sCncRange,kMapRange,inflation,...
                  nObsTime,nxObs,nyObs,nzObs)];

imageDir = [dataDir 'inversionImage/'];
mkdir(imageDir)

iu = 0:10;%[0:1:4];
load([dataDir 'update_Iter0.mat'],'Xtrue','kTrue','xRange','obsLoc')   
for it = 1:length(iu)
    fprintf('kVar:%.2f seed:%d M:%.1f iter:%d/%d \n',...
                            kVar,itrue,M,iu(it),iu(end));
    flName = sprintf([dataDir 'update_Iter%d.mat'],iu(it));
    load(flName,'Xe','ke')   
    checkFig
    suptitle(sprintf(['kVar%.2f M%.3f (%s data) @ Iter=%d'], ...
        kVar,M,figTxt,iu(it))); 
    drawnow
    print(sprintf([imageDir 'inversion_Iter%d.png'],iu(it)),'-dpng');
end
%% ---------------------------------------------------------------------
% video
%----------------------------------------------------------------------
videoDir = [codeDir 'video/'];
mkdir(videoDir)

fprintf('video kVar:%.2f seed:%d M:%.1f \n',kVar,itrue,M);
%fprintf('Video M = %.3f  \n',M);

videoName = [videoDir ...
            sprintf('inversion_kVar%.2f_seed%d_M%.1f.mp4',kVar,itrue,M)];
outputVideo = VideoWriter(videoName);
outputVideo.FrameRate = 2;
open(outputVideo)
for it = 1:length(iu)
    figName = sprintf([imageDir 'inversion_Iter%d.png'],iu(it));
    img = imread(figName);
    writeVideo(outputVideo,img);
end
close(outputVideo);

end
end
end
end
end
end
