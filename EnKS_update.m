disp(['| Assimilation']);

[~,obsLoc,nOutC,nOutP] = sub_obsSetting(M,obsType,nObsTime,nxObs,nyObs,nzObs);
% construct ensemble state matrix (A = [srcLoc; srcCnc; ke]) 
nstat = [3*nSrc; nSrc; numel(kTrue);];
A = [Xe; log(ke); ];

if M == 0 && numel(sd) == 2
    sig = [ones(nOutC,1)*sd(1); ones(nOutP,1)*sd(2)]; sig = sig(:);
else
    sig = [ones(nOutC,1) ones(nOutP,1)].*sd; sig = sig(:);
end

nOut = nOutC+nOutP;
Cd = eye(nOut);
for i = 1:nOut
    Cd(i,i) = sig(i)^2;  % covariance of the measurement errors
end
mean_x = repmat(mean(A,2),1,Ne);
mean_y = repmat(mean(Ye,2),1,Ne);
Cxy =  (A - mean_x)*(Ye - mean_y)'/(Ne - 1);
Cyy =  (Ye - mean_y)*(Ye - mean_y)'/(Ne - 1);

%% rhoAD & rhoDD for tapering 
nObsLoc = length(obsLoc);
% ------------- update cross-covariance /w localization        
if sLocRange
    % [srcLoc srcIntnsty] -- obs
    srcLocX = mean(Xe(1:3:3*nSrcInit-2,:),2);
    srcLocY = mean(Xe(2:3:3*nSrcInit-1,:),2);
    srcLocZ = mean(Xe(3:3:3*nSrcInit,:),2);

    [~,ixSrc] = min(abs(xCells-srcLocX'));
    [~,iySrc] = min(abs(yCells-srcLocY'));
    [~,izSrc] = min(abs(zCells-srcLocZ'));
    
    srcLocX = xCells(ixSrc);
    srcLocY = xCells(iySrc);
    srcLocZ = zCells(izSrc);
    
    rho_unit = zeros(size(Xe,1),nObsLoc);
    for iObsLoc = 1:nObsLoc
        do = sqrt((srcLocX - obsLoc(iObsLoc,1)).^2 ...
                + (srcLocY - obsLoc(iObsLoc,2)).^2 ...
                + (srcLocZ - obsLoc(iObsLoc,3)).^2);
        tau_sLoc = gaspari(do(:),sLocRange);
        tau_sCnc = gaspari(do(:),sCncRange);
        tmp = [tau_sLoc tau_sLoc tau_sLoc]'; 
        rho_unit(:,iObsLoc) = [tmp(:); tau_sCnc;];
    end
    rho_sy = repmat(rho_unit, 1, nOut/nObsLoc);
else
    rho_sy = ones(size(Xe,1),nOut);
end
   
if kMapRange
    % k field -- obs
    rho_unit = zeros(size(ke,1),nObsLoc);
    for iObsLoc = 1:nObsLoc
        do = sqrt((xxCells - obsLoc(iObsLoc,1)).^2 ...
                + (yyCells - obsLoc(iObsLoc,2)).^2 ...
                + (zzCells - obsLoc(iObsLoc,3)).^2);
        rho_unit(:,iObsLoc)= gaspari(do(:),kMapRange);
    end
    rho_ky = repmat(rho_unit, 1, nOut/nObsLoc);
else
    rho_ky = ones(size(ke,1),nOut);
end
rho_xy = [rho_sy; rho_ky];
    
% ------------- update auto-covariance w/ localization      
if sLocRange || sCncRange || kMapRange
    yRange = max([sLocRange sCncRange kMapRange]);
    rho_unit = zeros(nObsLoc,nObsLoc);
    for iObsLoc = 1:nObsLoc
        do = sqrt(sum((obsLoc - obsLoc(iObsLoc,:)).^2,2));
        rho_unit(:,iObsLoc)= gaspari(do(:),yRange);
    end
    rho_yy = kron(ones(nOut/nObsLoc),rho_unit);
else
    rho_yy = ones(size(Cyy));
end

%% Kalman gain + observation pertubation + update
kgain = (rho_xy .* Cxy) / (rho_yy .* Cyy + Cd);
obse = repmat(Ytrue,1,Ne) + normrnd(zeros(nOut,Ne),repmat(sig,1,Ne));
Au = A + kgain*(obse - Ye);

%% Covariance inflation
% ------------- srcLoc inflaiton
% ------------- srcCnc inflaiton
if inflation
    srcCnc = Au(nstat(1)+1:sum(nstat(1:2)),:);

    stdSrcCnc = std(srcCnc,[],2);
    ind = find(stdSrcCnc<0.2);

    if ~isempty(ind)
        meanSrcCnc = mean(srcCnc,2);

        tmp = meanSrcCnc + (srcCnc - meanSrcCnc)./stdSrcCnc * inflation;
        srcCnc(ind,:) = tmp(ind,:);

        Au(nstat(1)+1:sum(nstat(1:2)),:) = srcCnc;
        clear srcCnc stdSrcCnc ind meanSrcCnc tmp 
    else
        clear srcCnc stdSrcCnc ind 
    end
end
% ------------- kField inflaiton

%% boundary handling
Xe = Au(1:sum(nstat(1:end-1)),:);
ke = exp(Au(sum(nstat(1:end-1))+1:sum(nstat),:));
for i = 1:Ne
    for j = 1:size(xRange,1)
        if Xe(j,i) > xRange(j,2)
            Xe(j,i) = xRange(j,2);%(xRange(j,2) + Xe(j,i))/2;
        elseif Xe(j,i) < xRange(j,1)
            Xe(j,i) = xRange(j,1);%(xRange(j,1) + Xe(j,i))/2;
        end
    end
end

clear nstat A sig Cd mean_*  Cxy Cyy kgain obse Au
