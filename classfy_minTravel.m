function [Xe,iCntr] = classfy_minTravel(Xe,nSrcInit,C)

Ne = size(Xe,2);

xSrc = Xe([1:3:3*nSrcInit-2],:);
ySrc = Xe([2:3:3*nSrcInit-1],:);
zSrc = Xe([3:3:3*nSrcInit],:);

dist2cntrX = xSrc(:)-C(:,1)';
dist2cntrY = ySrc(:)-C(:,2)';
dist2cntrZ = zSrc(:)-C(:,3)';
dist2cntr = sqrt(dist2cntrX.^2 + dist2cntrY.^2 + dist2cntrZ.^2);

iCntr = nan(Ne*nSrcInit,1);
for ie = 1:Ne
    dist2cntrCase = dist2cntr(nSrcInit*(ie-1)+1:nSrcInit*ie,:);
    dist2cntrCaseT = dist2cntrCase';
    iCntrCase = nan(nSrcInit,1);
    
    allPerms = perms(1:nSrcInit);
    
    indAllPerms = nSrcInit*([1:nSrcInit]-1) + allPerms;
    [~,indPermsMinDist] = min(sum(dist2cntrCaseT(indAllPerms),2));
    
    iCntrCase = allPerms(indPermsMinDist,:);
    
    iCntr(nSrcInit*(ie-1)+1:nSrcInit*ie) = iCntrCase; %figure purpose
    
    Xe((iCntrCase-1)*3+1,ie) = xSrc(:,ie); % x srcLoc
    Xe((iCntrCase-1)*3+2,ie) = ySrc(:,ie); % y srcLoc
    Xe((iCntrCase-1)*3+3,ie) = zSrc(:,ie); % z srcLoc
    Xe(nSrcInit*3+iCntrCase,ie) = Xe(nSrcInit*3+1:nSrcInit*4,ie); % src intensity
    
end


% clf
% subplot(211);
% scatter3(xSrc(:),ySrc(:),zSrc(:),15,repmat([1:3]',size(Xe,2),1),'filled')
% xSrc = Xe([1:3:3*nSrcInit-2],:);
% ySrc = Xe([2:3:3*nSrcInit-1],:);
% zSrc = Xe([3:3:3*nSrcInit],:);
% subplot(212); 
% scatter3(xSrc(:),ySrc(:),zSrc(:),15,repmat([1:3]',size(Xe,2),1),'filled')


return

