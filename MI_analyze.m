clear all; clc; %clf; % gcp

codeDir = '~/sourceID/EnKS_sourceID/';

nSrc = 3;
nSrcInit = 3;%:10
Ne = 500;

loclzd = [0 1; 0 1];

varioType = ['Sphrc']';% 'Expnt'; 'Gausn']';

% txtObs=sprintf(['prf%d'], 10);
txtObs=sprintf(['cnc%.3f_prf%d'], 0.02,10);

nCase = 0;

sLoclzd = 1;
kLoclzd = 1;

clstr = 1;

nCase = 0;

M = [0 0.01 0.1 1 5 10];%[0 0.01 0.1  1 5 10]%
seed = [527 537 563 583 603 709 721 727 851 973];

prfStd = zeros(numel(seed),numel(M));
mutualInfo = zeros(numel(seed),numel(M));

nObsZ = 4; nObsX = 8;
mutualInfoLine = zeros(numel(seed),numel(M),nObsZ);
lineInd = reshape(1:nObsZ*nObsX,nObsZ,nObsX);

% lineInd = reshape(reshape(1:nObsZ*nObsX,nObsZ,nObsX)',16,2)';

wellId = 29;
for iVario = 1%:3
for iSeed = 1:numel(seed)
clf
for kVar = 0.5%[0.25 0.5]%[0.25 0.5 1 2 3]%[0.25 0.5 1 2 3]
for iM = 1:numel(M)%[0.01 0.1 1 10]%[0 0.01 0.1  1 5 10]%

    nCase = nCase + 1;
    
    dataDir = [codeDir ...
        sprintf(['%s/itrue%d/kVar%.2f/M%.3f/nSrc%d_nSrcInit%d_clstr%d/'...
                 'Ne%d_%s/sLoclzd%d_kLoclzd%d/'],...
                  varioType(:,iVario)',seed(iSeed),kVar,M(iM),nSrc,nSrcInit,clstr,...
                  Ne,txtObs,sLoclzd,kLoclzd)];
    
    load([dataDir sprintf('simData_Iter%d.mat',1)],'Ytrue')
    
    nObsLoc = 32; nObsTime = 10;
    nOutC = nObsLoc*nObsTime;
        
    cncObs = reshape(Ytrue(1:nOutC),nObsLoc,nObsTime);
    prfObs = Ytrue(nOutC+1:end);
    if M(iM) == 0
        prfObs = repmat(Ytrue(nOutC+1:end),1,nObsTime);
    else
        prfObs = reshape(Ytrue(nOutC+1:end),nObsLoc,nObsTime);
    end
    
    prfNorm = abs(prfObs-prfObs(:,1))./prfObs(:,1);
    subplot(211)
    plot([0.1:0.1:1],prfNorm(wellId,:),'o-'); hold on
    
    cncNorm = abs(cncObs-cncObs(:,1))./abs(cncObs(:,1));
    subplot(212)
    plot([0.1:0.1:1],cncNorm(wellId,:),'o-'); hold on
    
    prfStd(iSeed,iM) = mean(std(abs(diff(prfNorm,[],2)),[],2));
    cncStd(iSeed,iM) = mean(std(abs(diff(cncNorm,[],2)),[],2));
    
    L = 2^5;
    mutualInfo(iSeed,iM) = mi(prfObs,cncObs,L);
    for iObsZ = 1:size(lineInd,1)
        mutualInfoLine(iSeed,iM,iObsZ) = mi(prfObs(lineInd(iObsZ,:),:),cncObs(lineInd(iObsZ,:),:),L);
    end
    
%     mutualInfo(iSeed,iM) = mi(prfNorm,cncNorm);
%     for iObsZ = 1:nObsZ
%         mutualInfoLine(iSeed,iM,iObsZ) = mi(prfNorm(lineInd(iObsZ,:),:),cncNorm(lineInd(iObsZ,:),:));
%     end
end
legend(num2str(M'));
title(sprintf('iSeed: %d  wellId: %d', iSeed, wellId));

end
end
end
fprintf('nCase = %d \n',nCase')

%%
ticksM = [0.0001 0.01 0.1 1 5 10];

clf; 
subplot(211)
x = log10(ticksM);
y = mean(prfStd);
err = std(prfStd);
errorbar(x,y,err)

ylabel('data variability')
xlabel('M')
xticks(x);
xticklabels(num2str(M'))
xlim([-4.2 1.2])
ylim([0 1.6e-5])


subplot(212)
x = log10(ticksM);
y = mean(mutualInfo);
err = std(mutualInfo);
% errorbar(x,y,err); hold on

for iz = 1:nObsZ
    y = mean(mutualInfoLine(:,:,iz));
    err = std(mutualInfoLine(:,:,iz));
%     errorbar(x,y,err); hold on
    plot(x,y,'-o'); hold on
end
ylabel('mutual Info.')
xlabel('M')
xticks(x);
xticklabels(num2str(M'))
xlim([-4.2 1.2])

legend({'1','2','3','4'},'location','northwest')

%%






















