%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           EnKF_Main.m                                 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
%  Developed by:        
%       Seonkyoo Yoon < yoonx213@umn.edu >                     
% 
%  DESCRIPTION:   
%       An EnKF Application for conditioning log-permeability parameter to
%       pressure/concentration data in a density-variant groundwater system                        
% 
%  ASSUMPTIONS:  
%       isotropic permeability: kx = ky = kz
% 
%  GEOMETRIC INDEXING:                                                           
%    % The indice are arranged like this:                             
%    %         IndU                   +y (j)                             
%    %           | IndB               /                               
%    %           |/                  /                                
%    %  IndL -- Ind -- IndR         /---- +x (i)                        
%    %          /|                  |                                 
%    %      IndF |                  |                                 
%    %         IndD                +z (k)                              
%    %                                                                
%    % priority: z > x > y   [in MRST x > y > z]
%    %                                                               
%    % example:                                                      
%    %              ____19 22 25                                    
%    %       ____10 13 16 |23 26                                    
%    %     1  4  7 |14 17 |24 27                                    
%    %     2  5  8 |15 18 |                                         
%    %     3  6  9 |                                                
% 
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clc; %clf; % gcp
addpath('~/sourceID/EnKS_sourceID/');

caseRecog    % setting: seed, Ncore, Iter, Ne

%% ------------------------------------------------------------------------
% Initial Settings
% -------------------------------------------------------------------------
input_gridStructure       % Grid structure
input_physicalConst
% input_forwardModelSetting % for tObs, nOut

flName = sprintf([dataDir 'update_Iter%d.mat'],0);
if exist(flName,'file')
    load(flName,'Xe','Ye','ke','Xtrue','Ytrue','kTrue','xRange','obsLoc','kTrueMean')
else
    % ---------------------------------------------------------------------
    % K-Field generateion
    % ---------------------------------------------------------------------
    if ny > 1
        load([kMatDir '3DKmat_var1.mat'],'Kmat')
        Kmat = reshape(Kmat,100,100,100,27);

        nEns = 0;
        for ie = 1:27
            for iy = 1:100/ny
                for ix = 1:100/nx
                    for iz = 1:100/nz
                        nEns = nEns + 1;
                        tmp = Kmat( (iz-1)*nz+1:iz*nz,...
                                    (ix-1)*nx+1:ix*nx,...
                                    (iy-1)*ny+1:iy*ny,ie);
                        kMat(:,nEns) = tmp(:);
                    end
                end
            end
        end
    else
        load([kMatDir 'Kmat_var1.mat'],'Kmat')
        Kmat = reshape(Kmat,50,500,301);
        for iEns = 1:301
            for iFrme = 1:5
                tmp  = Kmat(:,(iFrme-1)*100+1:iFrme*nx,iEns);
                kMat(:,(iEns-1)*5+iFrme) = tmp(:);
            end
        end
    end
    clear Kmat
    kTrue = kMat(:,itrue);
    ke = kMat(:,1:Ne);

    kTrue = exp(log(kTrue)/mean(log(kTrue))*log(kTrueMean));
    kTrue = exp((log(kTrue)-log(kTrueMean))/std(log(kTrue))*sqrt(kVar)+log(kTrueMean));

    ke = exp(log(ke)./mean(log(ke))*log(kTrueMean));
    ke = exp((log(ke)-log(kTrueMean))./std(log(ke))*sqrt(kVar)+log(kTrueMean));

    % ---------------------------------------------------------------------
    % true parameter (X) & observation (Y) generation
    % ---------------------------------------------------------------------
    % ------ true X generation -------------
    Xtrue = nan(4*nSrc,1); % [srcLoc, srcCnc, k]
    Xtrue(1:nSrc*3,1) = [11; 17; 5; ...    xSrc1; ySrc1; zSrc1;
                         4; 9; 12; ...    xSrc2; ySrc2; zSrc2;
                         4; 17; 20];%      xSrc3; ySrc3; zSrc3;
    % Xtrue(1:nSrc*3,1) = [11; 0.5; 10; ...    xSrc1; ySrc1; zSrc1;
    %                      25; 0.5; 24; ...    xSrc2; ySrc2; zSrc2;
    %                      15; 0.5; 38];%      xSrc3; ySrc3; zSrc3;

    %Xtrue(3*nSrc+1:end,1) = [0.68; 0.83; 0.5; ];
    %Xtrue(3*nSrc+1:end,1) = [0.83; 0.5; 0.68; ];
%     Xtrue(3*nSrc+1:end,1) = [0.68; 0.5; 0.83; ];
%    Xtrue(3*nSrc+1:end,1) = [0.9; 0.75; 0.6; ];
    Xtrue(3*nSrc+1:end,1) = [1; 1; 1; ];

    % ------ true Y generation -------------
    fprintf('initial simulation for Ytrue \n');
    Ytrue = sub_forwardModel(Xtrue(1:3*nSrc),Xtrue(3*nSrc+1:end),kTrue,...
                                            sprintf(['simData_Iter%d'],0));
    Ytrue = Ytrue(:);

    % -------------------------------------------------------------------------
    % parameter ensemble generation: Xe=[srcLoc; srcHistory; K field]
    % -------------------------------------------------------------------------
    % ------ ensemble Xe generation -------------
    xRange = nan(length(Xtrue),2); 
    
    % range: srcLoc 
    for iSrc = 1:nSrcInit
%         xRange(iSrc*3-2:iSrc*3,:) = [2 33; 0.5 0.5; 2 48];
%         xRange(iSrc*3-2:iSrc*3,:) = [2 8; 7 13; 2 23];
        xRange(iSrc*3-2:iSrc*3,:) = [2 14; 7 19; 2 23];
    end
    % range: srcLoc intensity 
    xRange(3*nSrcInit+1:end,1) = ones(nSrcInit,1); %zeros(nSrcInit,1);
    xRange(3*nSrcInit+1:end,2) = ones(nSrcInit,1);
    
    Xe = nan(length(Xtrue),Ne); 
    for ie = 1:Ne % srcLoc generate
        Xe(1:end,ie) = unifrnd(xRange(1:end,1),xRange(1:end,2));
    end


    if doClstr
        xSrc = Xe([1:3:3*nSrcInit-2],:);
        ySrc = Xe([2:3:3*nSrcInit-1],:);
        zSrc = Xe([3:3:3*nSrcInit],:);

        srcXYZ = [xSrc(:) ySrc(:) zSrc(:)];

        [~,C] = kmeans(srcXYZ,nSrcInit);

        rng(itrue)
        [Xe,iCntr] = classfy_minTravel(Xe,nSrcInit,C);
        % classfy_minimum1st; figTitle = 'minimum1st';
        % classfy_farest1st; figTitle = 'farest1st';
    end
    
%     checkFig
    
    % ------ ensemble Ye generation -------------
    [~,obsLoc,nOutC,nOutP] = sub_obsSetting(M,obsType,nObsTime,nxObs,nyObs,nzObs);
    Ye = nan(nOutC+nOutP,Ne);

    save(flName,'Xe','Ye','ke','Xtrue','Ytrue','kTrue','xRange','obsLoc','kTrueMean')
end

 
%% ------------------------------------------------------------------------
% loop over entire time domain
%--------------------------------------------------------------------------
if nCpu > 1
    poolobj = gcp('nocreate'); % If no pool, do not create new one.
    if isempty(poolobj)
        fprintf('%d cores to be created. \n',nCpu);
        parpool('local',nCpu);
        delete(poolobj);
    end
    check_nCpu
end

do1stSim = true;
for iu = 1:Iter
    flNameOld = sprintf([dataDir 'update_Iter%d.mat'],iu-1);
    flName = sprintf([dataDir 'update_Iter%d.mat'],iu);
%    flNameJobOn = sprintf([dataDir 'simData_Iter%d_jobId%d.on'],iu,jobId);
    
    if exist(flNameOld,'file') && ~exist(flName,'file') %&& ~exist(flNameOcc,'file') 
        for ie = 1:Ne
            obsDataFlList{ie} = sprintf(['simData_Iter%d_ie%d'],iu,ie);
        end
        load(flNameOld,'Xe','ke')
       % ------------------------------------------------------------------
       % Forecast
       %-------------------------------------------------------------------
%        fidJobOn = fopen(flNameJobOn, 'a'); fclose(fidJobOn);    
        
        if do1stSim
            do1stSim = false;
            fprintf('pause %d sec. for different between jobIds.\n',jobId*2);
            pause(jobId*2); %

        end
        
        if nCpu > 1
            check_nCpu
            parfor ie = 1 : Ne
                if ~exist(flName,'file') ...
                   && ~exist([dataDir obsDataFlList{ie} '.mat'],'file') ...
                   && ~exist([dataDir obsDataFlList{ie} '.occ'],'file')

                    fprintf('ie:%d/%d | Iter:%d/%d \n', ie,Ne,iu,Iter);
                    sub_forwardModel(Xe(1:3*nSrcInit,ie),... srcLoc
                                     Xe(3*nSrcInit+1:end,ie),... srcCnc
                                     ke(:,ie),obsDataFlList{ie});% K-field
                end
            end
        else
            for ie = 1 : Ne
                if ~exist(flName,'file') ...
                   && ~exist([dataDir obsDataFlList{ie} '.mat'],'file') ...
                   && ~exist([dataDir obsDataFlList{ie} '.occ'],'file')

                    fprintf('ie:%d/%d | Iter:%d/%d \n', ie,Ne,iu,Iter);
                    sub_forwardModel(Xe(1:3*nSrcInit,ie),... srcLoc
                                     Xe(3*nSrcInit+1:end,ie),... srcCnc
                                     ke(:,ie),obsDataFlList{ie});% K-field
                end
            end
            
        end
        doUpdate = false;
        nSimData = 0;
        for ie = 1 : Ne
            if exist([dataDir obsDataFlList{ie} '.mat'],'file')
                nSimData = nSimData + 1;
            end
        end
        if nSimData == Ne; doUpdate = true; end

        while ~exist(flName,'file') && ~doUpdate
            fprintf('pause 10 sec. to wait until update. \n');
            pause(10)
        end
        
       % ------------------------------------------------------------------
       % Assimilation
       % ------------------------------------------------------------------
       if doUpdate
            for ie = 1 : Ne
                load([dataDir obsDataFlList{ie} '.mat'],'Y')
                Ye(:,ie) = Y;
            end
            EnKS_update
            if doClstr == 2
                C(:,1) = mean(Xe([1:2:2*nSrcInit-1],:),2);
                C(:,2) = mean(Xe([2:2:2*nSrcInit],:),2);

                [Xe,iCntr] = classfy_minTravel(Xe,nSrcInit,C);
                %
                clf; clc
                plot([xRange(1,1) xRange(1,1)],[xRange(2,:)],'k'); hold on
                plot([xRange(1,2) xRange(1,2)],[xRange(2,:)],'k'); 
                plot([xRange(1,:)],[xRange(2,1) xRange(2,1)],'k'); 
                plot([xRange(1,:)],[xRange(2,2) xRange(2,2)],'k'); 

                xSrc = Xe([1:2:2*nSrcInit-1],:);
                ySrc = Xe([2:2:2*nSrcInit],:);
                gscatter(xSrc(:),ySrc(:),repmat([1;2;3],Ne,1),'bgm','.',2.1)

                gscatter(mean(Xe([1:2:2*nSrcInit-1],:),2),...
                         mean(Xe([2:2:2*nSrcInit],:),2),[1:3],'r','^')

                gscatter(Xtrue([1:2:2*nSrcInit-1]),Xtrue([2:2:2*nSrcInit]),...
                    [1:3],'k','^'); 

                legend off
                axis equal
                drawnow

            end
           % -----------------------------------------------------------------
           % Solution Save
           % ------------------------------------------------------------------
            if ~exist(flName,'file'); save(flName,'Xe','ke'); end
            for ie = 1 : Ne
                if exist([dataDir obsDataFlList{ie} '.mat'],'file')
                    delete([dataDir obsDataFlList{ie} '.mat'])
                end
            end
       else
           fprintf('pause %d sec. to complete data storage.\n',jobId*2+10);
           pause(jobId*2+10); %
       end
    end
    Ye = nan(size(Ye));
    clear Xe ke 
end
poolobj = gcp('nocreate'); % If no pool, do not create new one.
delete(poolobj);
quit;

