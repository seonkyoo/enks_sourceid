%% Transmissibility Matrix
function [T,Lmat,invDmat,perm] = sub_transMat(M,kMat)

input_physicalConst     % physical constants 
input_gridStructure     % Grid structure

lMat = kMat./visco./phi; % mobility matrix

Tz = 2./( 1./lMat(2:nz,:,:) + 1./lMat(1:nz-1,:,:) ) * dx*dy/dz;
Tx = 2./( 1./lMat(:,2:nx,:) + 1./lMat(:,1:nx-1,:) ) * dy*dz/dx;
Ty = 2./( 1./lMat(:,:,2:ny) + 1./lMat(:,:,1:ny-1) ) * dz*dx/dy;

T.U = cat(1, zeros(1,nx,ny), Tz); TU=T.U(:); % no flow B.C. at the top 
T.D = cat(1, Tz, zeros(1,nx,ny)); TD=T.D(:); % no flow B.C. at the bottom 

T.L = cat(2, zeros(nz,1,ny), Tx);  TL=T.L(:); 
T.R = cat(2, Tx, zeros(nz,1,ny));  TR=T.R(:); 

T.F = cat(3, zeros(nz,nx,1), Ty); TF=T.F(:); % no flow B.C. at the front 
T.B = cat(3, Ty, zeros(nz,nx,1)); TB=T.B(:); % no flow B.C. at the back 

[Uin, dP, ~] = sub_boundaryCond(M);
if Uin == 0 && dP ~= 0 % pressure inlet B.C., assuming inf Tx at inlet
    T.L = cat(2, 2*dz*dy/dx*lMat(:,1,:), Tx);
end
T.R = cat(2, Tx, 2*dz*dy/dx*lMat(:,nx,:));  % hydrostatic B.C. at the outlet, assuming inf Tx at outlet

TC = T.L(:) + T.R(:) + T.U(:) + T.D(:) + T.F(:) + T.B(:);
Tmat = spdiags([-TB -TR -TD TC -TU -TL -TF],...
                          [-nz*nx,-nz,-1,0,1,nz,nz*nx],nz*nx*ny,nz*nx*ny)';
clear TB TR TD TC TU TL TF

[Lmat, Dmat, perm] = ldl(Tmat,'vector');
invDmat = 1./diag(Dmat);

return