
trueSrc = [1; 2; 3];
C=reshape(Xtrue(1:3*nSrc),3,nSrc)';
[~,ensSrc] = classfy_minTravel(mean(Xe,2),nSrcInit,C);

clf
set(gcf,'color','w')
symb = '^*d';

% colr = 'ygc'; cVec= [1 1 1; 0 1 0; 0 1 1];
colr = 'mgc'; cVec= [1 0 1; 0 1 0; 0 1 1];
Colr = repmat(cVec(ensSrc,:),Ne,1);

clf
%% src Intensity graph
subplot(2,12,11:12)

ensMean = mean(Xe(3*nSrc+ensSrc,:),2);
ensStd =  std(Xe(3*nSrc+ensSrc,:)');
% ensMean = mean(Xe(3*nSrc+[1:3],:),2);
% ensStd =  std(Xe(3*nSrc+[1:3],:)');

for ii = 1:numel(trueSrc)
    errorbar(trueSrc(ii), ensMean(ii), 1*ensStd(ii),symb(ii),'color','k'); hold on
%     errorbar(trueSrc(ii), ensMean(ensSrc(ii)), 1*ensStd(ensSrc(ii)),symb(ensSrc(ii)),'color','k'); hold on
    plot(trueSrc(ii),Xtrue(3*nSrc+trueSrc(ii)),symb(ii),'color','r')
end
xlim([0.8 3.2])
ylim([-0.05 1.02])
set(gca,'xtick',trueSrc)
ylabel('Source intensity')
xlabel('Source location')




%% src Location figures
xSrcE = Xe([1:3:3*nSrcInit-2],:);
ySrcE = Xe([2:3:3*nSrcInit-1],:);
zSrcE = Xe([3:3:3*nSrcInit],:);

xSrcTrue = Xtrue([1:3:3*nSrcInit-2]);
ySrcTrue = Xtrue([2:3:3*nSrcInit-1]);
zSrcTrue = Xtrue([3:3:3*nSrcInit]);

xSrcMean = mean(Xe([1:3:3*nSrcInit-2],:),2);
ySrcMean = mean(Xe([2:3:3*nSrcInit-1],:),2);
zSrcMean = mean(Xe([3:3:3*nSrcInit],:),2);

%% srcLoc in 3D space
subplot(2,12,13:16)

scatter3(xSrcE(:),zSrcE(:),ySrcE(:),1,Colr,'filled'); hold on

for iSrc = 1:nSrc
    scatter3(xSrcTrue(iSrc),zSrcTrue(iSrc),ySrcTrue(iSrc),50,'r',symb(iSrc),'linewidth',2); 
    scatter3(xSrcMean(iSrc),zSrcMean(iSrc),ySrcMean(iSrc),50,'k',symb(ensSrc(iSrc)),'linewidth',2); 
end

axis equal tight 

xlim(xRange(1,:))
ylim(xRange(3,:))
zlim(xRange(2,:))

plot3(xRange(1,:),xRange(3,[1 1]),xRange(2,[1 1]),'k')
plot3(xRange(1,[end end]),xRange(3,:),xRange(2,[1 1]),'k')
plot3(xRange(1,[end end]),xRange(3,[end end]),xRange(2,:),'k')


legend off         

xlabel('x')
ylabel('z')
zlabel('y')

set(gca,'ydir','reverse')
set(gca,'zdir','reverse')
% grid off

% zoom(2)
% view(20,30)
view(-60,50)
% view(10,-80)
% view(30,-70)
% view(3)

% view(12,-45)



%% srcLoc on x-y plane
subplot(2,12,18:19); hold on

gscatter(xSrcE(:),ySrcE(:),repmat(ensSrc,Ne,1),colr,'.',2); legend off         

gscatter(xSrcTrue(:),ySrcTrue(:),[1:3]','r','^*d',10); legend off         

gscatter(xSrcMean(:),ySrcMean(:),ensSrc,'k','^*d',10); legend off         

axis equal tight 


xlabel('x')
ylabel('y')

xlim(xRange(1,:))
ylim(xRange(2,:))



%% srcLoc on y-z plane
subplot(2,12,21:22); hold on

gscatter(ySrcE(:),zSrcE(:),repmat(ensSrc,Ne,1),colr,'.',2); legend off         

gscatter(ySrcTrue(:),zSrcTrue(:),[1:3]','r','^*d',10); legend off         

gscatter(ySrcMean(:),zSrcMean(:),ensSrc,'k','^*d',10); legend off         

axis equal tight 


xlabel('y')
ylabel('z')

xlim(xRange(2,:))
ylim(xRange(3,:))

set(gca,'ydir','reverse')

%% srcLoc on x-z plane
subplot(2,12,23:24); hold on

gscatter(xSrcE(:),zSrcE(:),repmat(ensSrc,Ne,1),colr,'.',2); legend off         


gscatter(xSrcTrue(:),zSrcTrue(:),[1:3]','r','^*d',10); legend off         

gscatter(xSrcMean(:),zSrcMean(:),ensSrc,'k','^*d',10); legend off         

axis equal tight 


xlabel('x')
ylabel('z')

xlim(xRange(1,:))
ylim(xRange(3,:))

set(gca,'ydir','reverse')
%%
subplot(2,12,1:4)
if ny == 1
    imagesc(xCells,zCells,reshape(log(kTrue),nz,nx)); hold on
else
    xslice = xCells([1 end]);    % location of y-z planes
    yslice = yCells([1 end]);              % location of x-z plane
    zslice = zCells([1 end]);         % location of x-y planes
    % 
    h = slice(xxCells,zzCells,yyCells,reshape(log(kTrue),nz,nx,ny),xslice,zslice,yslice);
    hold on
    axis equal tight

    xlabel('x')
    ylabel('z')
    zlabel('y')
    set(h,'edgecolor','none')

    set(gca,'ydir','reverse')
    set(gca,'zdir','reverse')
    view(-40,60)
end

plot3(xCells([1 end]),zCells([1 1]),yCells([1 1]),'k')
plot3(xCells([end end]),zCells([1 1]),yCells([1 end]),'k')
plot3(xCells([end end]),zCells([1 end]),yCells([1 1]),'k')


cl = caxis;
legend off
axis equal tight
colormap('parula')
title(sprintf('true (seed:%d)',itrue))


set(gca,'ydir','normal')
set(gca,'zdir','normal')
view(12,-45)
% view(15,-70)

%%
% clf
subplot(2,12,6:9)

if ny == 1
    imagesc(xCells,zCells,reshape(mean(log(ke),2),nz,nx)); hold on
else
    % 
    h = slice(xxCells,zzCells,yyCells,reshape(mean(log(ke),2),nz,nx,ny),xslice,zslice,yslice);
    hold on

    xlabel('x')
    ylabel('z')
    zlabel('y')
    set(h,'edgecolor','none')

    set(gca,'ydir','reverse')
    set(gca,'zdir','reverse')
    view(-40,60)
end
axis equal tight
caxis(cl)

plot3(xCells([1 end]),zCells([1 1]),yCells([1 1]),'k')
plot3(xCells([end end]),zCells([1 1]),yCells([1 end]),'k')
plot3(xCells([end end]),zCells([1 end]),yCells([1 1]),'k')

legend off
axis equal tight
colormap('parula')

title(sprintf('Ensemble Mean'))

legend off

set(gca,'ydir','normal')
set(gca,'zdir','normal')
view(12,-45)



%% clf
% 
% subplot(2,12,23:24)
% 
% 
% xSrcE = Xe([1:3:3*nSrcInit-2],:);
% ySrcE = Xe([2:3:3*nSrcInit-1],:);
% zSrcE = Xe([3:3:3*nSrcInit],:);
% scatter3(xSrcE(:),zSrcE(:),ySrcE(:),1,repmat(ensSrc,size(Xe,2),1),'filled'); 
% hold on
% 
% xSrcTrue = Xtrue([1:3:3*nSrcInit-2]);
% ySrcTrue = Xtrue([2:3:3*nSrcInit-1]);
% zSrcTrue = Xtrue([3:3:3*nSrcInit]);
% scatter3(xSrcTrue(:),zSrcTrue(:),ySrcTrue(:),30,[1:3]','^','filled'); 
% 
% xSrcMean = mean(Xe([1:3:3*nSrcInit-2],:),2);
% ySrcMean = mean(Xe([2:3:3*nSrcInit-1],:),2);
% zSrcMean = mean(Xe([3:3:3*nSrcInit],:),2);
% scatter3(xSrcMean(:),zSrcMean(:),ySrcMean(:),30,ensSrc,'o','filled'); 
% 
% 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,:)],[xRange(2,1) xRange(2,1)],'g'); hold on
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,:)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,:)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,:)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,:)],[xRange(3,1) xRange(3,1)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,:)],[xRange(3,1) xRange(3,1)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,:)],[xRange(3,2) xRange(3,2)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,:)],[xRange(3,2) xRange(3,2)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,1) xRange(3,1)],[xRange(2,:)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,1) xRange(3,1)],[xRange(2,:)],'g'); 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,2) xRange(3,2)],[xRange(2,:)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,2) xRange(3,2)],[xRange(2,:)],'g'); 
% 
% 
% 
% legend off         
% 
% % gscatter(mean(Xe([1:3:3*nSrcInit-2],:),2),...
% %             mean(Xe([2:2:2*nSrcInit],:),2),ensSrc,'k','^*d',10)
% 
% xlabel('x')
% ylabel('z')
% zlabel('y')
% 
% axis equal tight 
% % grid off
% 
% % zoom(2)
% % view(20,30)
% % view(-5,20)
% view(0,-90)
% 
% 
% %% clf
% 
% subplot(2,12,21:22)
% 
% 
% xSrcE = Xe([1:3:3*nSrcInit-2],:);
% ySrcE = Xe([2:3:3*nSrcInit-1],:);
% zSrcE = Xe([3:3:3*nSrcInit],:);
% scatter3(xSrcE(:),zSrcE(:),ySrcE(:),1,repmat(ensSrc,size(Xe,2),1),'filled'); 
% hold on
% 
% xSrcTrue = Xtrue([1:3:3*nSrcInit-2]);
% ySrcTrue = Xtrue([2:3:3*nSrcInit-1]);
% zSrcTrue = Xtrue([3:3:3*nSrcInit]);
% scatter3(xSrcTrue(:),zSrcTrue(:),ySrcTrue(:),30,[1:3]','^','filled'); 
% 
% xSrcMean = mean(Xe([1:3:3*nSrcInit-2],:),2);
% ySrcMean = mean(Xe([2:3:3*nSrcInit-1],:),2);
% zSrcMean = mean(Xe([3:3:3*nSrcInit],:),2);
% scatter3(xSrcMean(:),zSrcMean(:),ySrcMean(:),30,ensSrc,'o','filled'); 
% 
% 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,:)],[xRange(2,1) xRange(2,1)],'g'); hold on
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,:)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,:)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,:)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,:)],[xRange(3,1) xRange(3,1)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,:)],[xRange(3,1) xRange(3,1)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,:)],[xRange(3,2) xRange(3,2)],[xRange(2,1) xRange(2,1)],'g'); 
% plot3([xRange(1,:)],[xRange(3,2) xRange(3,2)],[xRange(2,2) xRange(2,2)],'g'); 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,1) xRange(3,1)],[xRange(2,:)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,1) xRange(3,1)],[xRange(2,:)],'g'); 
% plot3([xRange(1,1) xRange(1,1)],[xRange(3,2) xRange(3,2)],[xRange(2,:)],'g'); 
% plot3([xRange(1,2) xRange(1,2)],[xRange(3,2) xRange(3,2)],[xRange(2,:)],'g'); 
% 
% 
% 
% legend off         
% 
% % gscatter(mean(Xe([1:3:3*nSrcInit-2],:),2),...
% %             mean(Xe([2:2:2*nSrcInit],:),2),ensSrc,'k','^*d',10)
% 
% xlabel('x')
% ylabel('z')
% zlabel('y')
% 
% axis equal tight 
% % grid off
% 
% % zoom(2)
% % view(20,30)
% % view(-5,20)
% view(90,-00)
% % set(gca,'Rotation',90)
% 
% 
