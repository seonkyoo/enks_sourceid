clear all; clc; clf; % gcp

input_gridStructure     % Grid structure

codeDir = '/panfs/roc/groups/6/pkkang/yoonx213/sourceID/EnKS_sourceID/';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

resultDir = '~/sourceID/EnKS_sourceID/result/';
mkdir(resultDir)

mksize0 = 10;
mksize1 = 5;
mksize2 = 10;

symb = '^*dsx+h><p';

trueSrc = [1; 2; 3];



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% variable combinations
clc

nSrc = 3;
nSrcInit = 3;
clstr = [0 1];

Ne = 500;

varioType = 'Sph3d';
seed = [502 527 537 563 583 603 709 721 727 851];
kVar = [0.25 0.5 1 2 3];
M = [0.1  1 10];
Iter = 10;

sLocRange = [0 25];
sCncRange = sLocRange;
kMapRange = sLocRange;
inflation = 0;

nObsTime = 10; nzObs = 5; nxObs = 5; nyObs = 4; 

nDataScenario = 1;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% VOI calculation

VOI_srcLocDistXYZ = nan(numel(clstr),numel(sLocRange),...
                        numel(kVar),numel(seed),numel(M),Iter,3);
VOI_srcLocStdXYZ = nan(numel(clstr),numel(sLocRange),...
                       numel(kVar),numel(seed),numel(M),Iter,3);

VOI_srcLocDist = nan(numel(clstr),numel(sLocRange),...
                     numel(kVar),numel(seed),numel(M),Iter);
VOI_srcLocStd = nan(numel(clstr),numel(sLocRange),...
                    numel(kVar),numel(seed),numel(M),Iter);

VOI_kFieldDistMean = nan(numel(clstr),numel(sLocRange),...
                         numel(kVar),numel(seed),numel(M),Iter);
VOI_kFieldStd = nan(numel(clstr),numel(sLocRange),...
                    numel(kVar),numel(seed),numel(M),Iter);

for iclstr = 1:numel(clstr)
for isLocRange = 1:numel(sLocRange)
    sCncRange = sLocRange(isLocRange);
    kMapRange = sLocRange(isLocRange);
for iDataScenario = 1
    if iDataScenario == 1
        txtObs=sprintf(['cnc%.3f_prf%d'], 0.02,10);
    elseif iDataScenario == 2
        txtObs=sprintf(['prf%d'], 10);
    end
for iSeed = 1:numel(seed)
for ikVar = 1:numel(kVar)
for iM = 1:numel(M) 
for iu = 1:Iter%[1 5:5:Iter]
    fprintf('clstr: %d, sLocRange: %d, iSeed: %d, ikVar:%d, iM:%d, iu:%d \n',...
            clstr(iclstr),sLocRange(isLocRange),iSeed,ikVar,iM,iu);
    dataDir = [codeDir ...
        sprintf(['%s/itrue%d/kVar%.2f/M%.3f/nSrc%d_nSrcInit%d_clstr%d/'...
                 'Ne%d_%s/sLocRange%d_sCncRange%d_kMapRange%d_inflation%.2f/'...
                 'nObsTime%d_nxObs%d_nyObs%d_nzObs%d/'],...
                  varioType,seed(iSeed),kVar(ikVar),M(iM),...
                  nSrc,nSrcInit,clstr(iclstr),...
                  Ne,txtObs,sLocRange(isLocRange),sCncRange,kMapRange,inflation,...
                  nObsTime,nxObs,nyObs,nzObs)];

    flNameVOI = sprintf([dataDir 'VOI_Iter%d.mat'],iu);
    if exist(flNameVOI,'file')
        load(flNameVOI);
    else
        load([dataDir 'update_Iter0.mat'],'Xe','ke','Xtrue','kTrue');%,'xRange','obsLoc'
        kMatRef = reshape(kTrue,nz,nx,ny);
        Xe0 = Xe;
        ke0 = ke;

        % ----------------------- ensSrc -----------------------
        flName = sprintf('%supdate_Iter%d.mat',dataDir,Iter);
        load(flName,'Xe')

        C=reshape(Xtrue(1:3*nSrc),3,nSrc)';
        [~,ensSrc] = classfy_minTravel(mean(Xe,2),nSrcInit,C);

        indSrc = [3*(ensSrc'-1)+1; 3*(ensSrc'-1)+2; 3*ensSrc']; 
        indSrc = indSrc(:);
        % ------------------------------------------------------ 

        flName = sprintf('%supdate_Iter%d.mat',dataDir,iu);
        load(flName,'Xe','ke')

        srcLocTrue = Xtrue(1:3*nSrc);
        srcLocMean0 = mean(Xe0(indSrc,:),2);
        srcLocMean = mean(Xe(indSrc,:),2);

        srcLocBias = reshape((srcLocTrue-srcLocMean).^2,3,nSrc);
        srcLocBias0 = reshape((srcLocTrue-srcLocMean0).^2,3,nSrc);
        
        srcLocDistXYZ  = sum(sqrt(srcLocBias ),2);
        srcLocDistXYZ0 = sum(sqrt(srcLocBias0),2);
        
        srcLocDist = sum(sqrt(sum(srcLocBias)));
        srcLocDist0 = sum(sqrt(sum(srcLocBias0)));
       
        srcLocStdXYZ = sum(reshape(std(Xe(indSrc,:),[],2),3,nSrc),2);
        srcLocStdXYZ0 = sum(reshape(std(Xe0(indSrc,:),[],2),3,nSrc),2);
        
        srcLocStd = sum(srcLocStdXYZ);
        srcLocStd0 = sum(srcLocStdXYZ0);

        kFieldDist = sqrt(sum((log(kTrue) - mean(log(ke),2)).^2));
        kFieldDist0 = sqrt(sum((log(kTrue) - mean(log(ke0),2)).^2));
        
        kFieldStd  = sum(std(log(ke),[],2));
        kFieldStd0 = sum(std(log(ke0),[],2));

        save(flNameVOI,'srcLocTrue','srcLocMean0','srcLocMean',...
        'srcLocDistXYZ','srcLocDistXYZ0','srcLocDist','srcLocDist0',...
        'srcLocStdXYZ','srcLocStdXYZ0','srcLocStd','srcLocStd0',...
        'kFieldDist','kFieldDist0','kFieldStd','kFieldStd0');
            
    end
    
    VOI_srcLocDistXYZ(iclstr,isLocRange,ikVar,iSeed,iM,iu,:) = ...
                    (srcLocDistXYZ0-srcLocDistXYZ)./srcLocDistXYZ0;
    VOI_srcLocStdXYZ(iclstr,isLocRange,ikVar,iSeed,iM,iu,:) = ...
                    (srcLocStdXYZ0-srcLocStdXYZ)./srcLocStdXYZ0;
    
    VOI_srcLocDist(iclstr,isLocRange,ikVar,iSeed,iM,iu) = (srcLocDist0-srcLocDist)/srcLocDist0;
    VOI_srcLocStd(iclstr,isLocRange,ikVar,iSeed,iM,iu) = (srcLocStd0-srcLocStd)/srcLocStd0;

    VOI_kFieldDistMean(iclstr,isLocRange,ikVar,iSeed,iM,iu) = (kFieldDist0-kFieldDist)/kFieldDist0;
    VOI_kFieldStd(iclstr,isLocRange,ikVar,iSeed,iM,iu) = (kFieldStd0-kFieldStd)/kFieldStd0;

end
end
end
end
end
end
end
%% x: M -- y: srcLoc & k field err reduction | lines: clstr & sLocRange %%%%
clf; clc

ticksM = M;
lineType = {'-',':','-','--'};
iClstr = 1;

indSeed =  [ 1  3 4 5 7 8 9 10] ;

for ikVar = 1:5%[1 2 3]%[3 4 5]%1:numel(kVar)
for iu = [5 10]%Iter
clf
leg = cell(0);
iLeg = 0;
for iclstr = 1:2
for isLocRange = 1:2


iLeg = iLeg + 1;
% leg{iLeg} = num2str(kVar(ikVar));
leg{iLeg} = sprintf('clstr%d locl%d',iclstr-1,isLocRange-1);

subplot(2,2,1);
plot(log10(ticksM),...
    mean(squeeze(VOI_srcLocDist(iclstr,isLocRange,ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('src Loc err reduction');
% ylim([0.1 0.7]);
xlim([-1.5 1.5])

if iclstr == 2 && isLocRange ==2
    legend(leg,'location','southeast');
    legend box off
end


subplot(2,2,2);
plot(log10(ticksM),...
    mean(squeeze(VOI_srcLocStd(iclstr,isLocRange,ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('src Loc std reduction');
% ylim([-0.1 1.1]);
xlim([-1.5 1.5])


subplot(2,2,3);
plot(log10(ticksM),...
    mean(squeeze(VOI_kFieldDistMean(iclstr,isLocRange,ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('k field err reduction');
% ylim([0.6 0.75]);
xlim([-1.5 1.5])

subplot(2,2,4);
plot(log10(ticksM),...
    mean(squeeze(VOI_kFieldStd(iclstr,isLocRange,ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('k field std reduction');
% ylim([-0.1 6]);
xlim([-1.5 1.5])
end
% suptitle(sprintf('localzd:%d kVar:%.2f %s iter:%d',iLoclzd,kVar(ikVar),txtObs,iu));
% suptitle(sprintf('clstr:%d kVar:%.2f %s iter:%d',iClstr,kVar,txtObs,iu));
% drawnow
% pause(0.5)
end
suptitle(sprintf('kVar:%.2f iter:%d',kVar(ikVar),iu));
set(gcf,'color','w')
fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 6 6];
print(sprintf([resultDir 'kVar%.2f_iter%d_M_errStdReduction.png'],...
              kVar(ikVar),iu),'-dpng');

end
end



%% x: kVar -- y: srcLoc & k field err reduction %%%%
clf; clc

set(gcf,'color','w');
ticksM = M;
lineType = {'-',':','-','--'};
iClstr = 1;

%indSeed =  [ 1 3 4 5 7 8 9 10] ; % clstr:1 sLocRange:25
indSeed =  [ 1 3 4 5 7 8 9 10] ;

for iM = 1:numel(M)
    subplot(1,2,1);
    plot(kVar,mean(squeeze(VOI_srcLocDist(:,indSeed,iM,iu)),2),'o-')
    hold on;
    xticks(kVar);
    % xticklabels(num2str(M'));
    xlabel('k field variance');
    ylabel('src Loc err reduction');
    % ylim([0.1 0.7]);
    % xlim([-1.5 1.5])



    subplot(1,2,2);
    plot(kVar,mean(squeeze(VOI_kFieldDistMean(:,indSeed,iM,iu)),2),'o-')
    hold on;
    xticks(kVar);
    % xticklabels(num2str(M'));
    xlabel('k field variance');
    ylabel('k filed err reduction');
%     ylim([0.25 0.43]);
    % xlim([-1.5 1.5])
end
legend(num2str(M'),'location','southeast')
legend box off

fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 7 4];
print(sprintf([resultDir 'clstr%d_sLocRange%d_kVar_errReduction_Iter%d.png'],clstr,sLocRange,iu),'-dpng');


%% x: kVar -- y: srcLoc & k field err reduction improvement %%%%
clf; clc

ticksM = M;
lineType = {'-',':','-','--'};
iClstr = 1;

indSeed =  [ 1 3 4 5 7 8 9 10] ;


subplot(1,2,1);
plot(kVar,mean(squeeze(VOI_srcLocDist(:,indSeed,end,iu)),2) - ...
mean(squeeze(VOI_srcLocDist(:,indSeed,1,iu)),2),'o-')
hold on;
xticks(kVar);
% xticklabels(num2str(M'));
xlabel('k field variance');
ylabel('src Loc err reduction improvement by M:10');
% ylim([0.1 0.7]);
% xlim([-1.5 1.5])



subplot(1,2,2);
plot(kVar,mean(squeeze(VOI_kFieldDistMean(:,indSeed,end,iu)),2) - ...
mean(squeeze(VOI_kFieldDistMean(:,indSeed,1,iu)),2),'o-')
hold on;
xticks(kVar);
% xticklabels(num2str(M'));
xlabel('k field variance');
ylabel('k filed err reduction improvement by M:10');
% ylim([0.1 0.7]);
% xlim([-1.5 1.5])

fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 8 4];
print(sprintf([resultDir 'clstr%d_sLocRange%d_kVar_errReduction_improvement.png'],clstr,sLocRange),'-dpng');


%% x: M -- y: srcLoc & k field err std reduction %%%%
clf; clc

ticksM = M;
lineType = {'-',':','-','--'};
iClstr = 1;

indSeed =  [ 1  3 4 5 7 8 9 10] ;

leg = cell(0);
for iu = 10%Iter
iLeg = 0;
for ikVar = 1:5%[1 2 3]%[3 4 5]%1:numel(kVar)
iLeg = iLeg + 1;
leg{iLeg} = num2str(kVar(ikVar));

subplot(2,2,1);
plot(log10(ticksM),...
    mean(squeeze(VOI_srcLocDist(ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('src Loc err reduction');
% ylim([0.1 0.7]);
xlim([-1.5 1.5])

if ikVar == 5
    legend(leg,'location','northwest');
    legend box off
end


subplot(2,2,2);
plot(log10(ticksM),...
    mean(squeeze(VOI_srcLocStd(ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('src Loc std reduction');
% ylim([-0.1 1.1]);
xlim([-1.5 1.5])


subplot(2,2,3);
plot(log10(ticksM),...
    mean(squeeze(VOI_kFieldDistMean(ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('k field err reduction');
% ylim([0.6 0.75]);
xlim([-1.5 1.5])

subplot(2,2,4);
plot(log10(ticksM),...
    mean(squeeze(VOI_kFieldStd(ikVar,indSeed,:,iu))),...
    ['o' lineType{iClstr}]); hold on;
xticks(log10(ticksM));
xticklabels(num2str(M'));
xlabel('M');
ylabel('k field std reduction');
% ylim([-0.1 6]);
xlim([-1.5 1.5])
end
% suptitle(sprintf('localzd:%d kVar:%.2f %s iter:%d',iLoclzd,kVar(ikVar),txtObs,iu));
% suptitle(sprintf('clstr:%d kVar:%.2f %s iter:%d',iClstr,kVar,txtObs,iu));
% drawnow
% pause(0.5)
end


fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 7 7];
print(sprintf([resultDir 'clstr%d_sLocRange%d_M_errStdReduction.png'],clstr,sLocRange),'-dpng');


%% x: M -- y: srcLocXYZ err std reduction %%%%
clf; clc

ticksM = M;
lineType = {'-',':','-','--'};
iClstr = 1;

indSeed =  [ 1  3 4 5 7 8 9 10] ;

leg = cell(0);
for iu = 10%Iter
iLeg = 0;
for ikVar = 1:5%[1 2 3]%[3 4 5]%1:numel(kVar)
iLeg = iLeg + 1;
leg{iLeg} = num2str(kVar(ikVar));

for iCoord = 1:3
    subplot(2,3,iCoord);
    plot(log10(ticksM),...
        mean(squeeze(VOI_srcLocDistXYZ(ikVar,indSeed,:,iu,iCoord))),...
        ['o' lineType{iClstr}]); hold on;
    xticks(log10(ticksM));
    xticklabels(num2str(M'));
    xlabel('M');
    ylabel('src Loc err reduction');
    ylim([0. 0.95]);
    xlim([-1.5 1.5])
    if iCoord == 3 && ikVar == 5
        legend(leg,'location','southeast');
        legend box off
    end
    title(sprintf('coordinates-%d',iCoord))




    subplot(2,3,iCoord+3);
    plot(log10(ticksM),...
        mean(squeeze(VOI_srcLocStdXYZ(ikVar,indSeed,:,iu,iCoord))),...
        ['o' lineType{iClstr}]); hold on;
    xticks(log10(ticksM));
    xticklabels(num2str(M'));
    xlabel('M');
    ylabel('src Loc std reduction');
    ylim([0.45 0.95]);
    xlim([-1.5 1.5])
    if iCoord == 3 && ikVar == 5
        legend(leg,'location','southeast');
        legend box off
    end
    title(sprintf('coordinates-%d',iCoord))

end

% subplot(2,2,2);
% plot(log10(ticksM),...
%     mean(squeeze(VOI_srcLocStd(ikVar,indSeed,:,iu))),...
%     ['o' lineType{iClstr}]); hold on;
% xticks(log10(ticksM));
% xticklabels(num2str(M'));
% xlabel('M');
% ylabel('src Loc std reduction');
% % ylim([-0.1 1.1]);
% xlim([-1.5 1.5])
% 
% 
% subplot(2,2,3);
% plot(log10(ticksM),...
%     mean(squeeze(VOI_kFieldDistMean(ikVar,indSeed,:,iu))),...
%     ['o' lineType{iClstr}]); hold on;
% xticks(log10(ticksM));
% xticklabels(num2str(M'));
% xlabel('M');
% ylabel('k field err reduction');
% % ylim([0.6 0.75]);
% xlim([-1.5 1.5])
% 
% subplot(2,2,4);
% plot(log10(ticksM),...
%     mean(squeeze(VOI_kFieldStd(ikVar,indSeed,:,iu))),...
%     ['o' lineType{iClstr}]); hold on;
% xticks(log10(ticksM));
% xticklabels(num2str(M'));
% xlabel('M');
% ylabel('k field std reduction');
% % ylim([-0.1 6]);
% xlim([-1.5 1.5])
end
% suptitle(sprintf('localzd:%d kVar:%.2f %s iter:%d',iLoclzd,kVar(ikVar),txtObs,iu));
% suptitle(sprintf('clstr:%d kVar:%.2f %s iter:%d',iClstr,kVar,txtObs,iu));
% drawnow
% pause(0.5)
end


fig = gcf;
fig.Units = 'inches';
fig.PaperPosition = [0 0 8 5];
print(sprintf([resultDir 'clstr%d_sLocRange%d_M_errStdReductionXYZ.png'],clstr,sLocRange),'-dpng');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clf

indSeed = 7;%[ 1  3 4 5 7 8 9 10] ;

tmp = squeeze(VOI_srcLocDist(:,indSeed,:,iu));
subplot(121)
for ikVar = 1:5
    plot(log10(ticksM),tmp(ikVar,:),'o-'); hold on;
    xticks(log10(ticksM));
    xticklabels(num2str(M'));
    xlabel('M');
    ylabel('src Loc err');
    % ylim([0.1 0.7]);
    xlim([-1.5 1.5])
end
legend(num2str(kVar'),'location','southeast')

tmp = squeeze(VOI_kFieldDistMean(:,indSeed,:,iu));
subplot(122)
for ikVar = 1:5
    plot(log10(ticksM),tmp(ikVar,:),'o-'); hold on;
    xticks(log10(ticksM));
    xticklabels(num2str(M'));
    xlabel('M');
    ylabel('k field err');
    % ylim([0.6 0.75]);
    xlim([-1.5 1.5])
    % ylim([0.1 0.7]);
    xlim([-1.5 1.5])
end
suptitle(sprintf('seed: %d (iSeed: %d)',seed(indSeed),indSeed ))

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for iSrc = 1:nSrc
%     subplot(nSrc,1,iSrc)
%     
%     ii = ensSrc(iSrc);
%     ensMean = mean(Xe(2*nSrc+ii,:),2);
%     ensStd =  std(Xe(2*nSrc+(ii-1)*10+1:2*nSrc+(ii)*10,:)');
%     
%     errorbar(tStep, ensMean, ensStd); hold on
%     
%     ii = trueSrc(iSrc);
%     plot(tStep,Xtrue(2*nSrc+(ii-1)*10+1:2*nSrc+(ii)*10),'o')
%     
%     ylim([-0.2 1.2])
%     title(sprintf('source %d',iSrc));
% %         [xp,xx] = ksdensity(Xe(i,:));
% %     plot(xx,xp,'b','linewidth',1.5);hold on;
% %     [xp2,xx2] = ksdensity(Xe(i,:));
% %     plot(xx2,xp2,'r','linewidth',1.5,'linestyle','--');
% %     plot([Xtrue(i) Xtrue(i)],[0 max([max(xp),max(xp2)])*1.2],'color','k','linewidth',1.5);
% %     drawnow;
% % end
%     ylabel('Source intensity')
% end
% xlabel('time [day]')



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clc; clf
% trueSrc = [1 2 3];
% ensSrc = [1 2 3];
% 
% for iSrc = 1:nSrc
%     subplot(nSrc,1,iSrc)
% % for i = 2*nSrc+1:5
%     tStep = [2.5:5:47.5];
%     ii = ensSrc(iSrc);
%     ensMean = mean(Xe(2*nSrc+(ii-1)*10+1:2*nSrc+(ii)*10,:),2);
%     ensStd =  std(Xe(2*nSrc+(ii-1)*10+1:2*nSrc+(ii)*10,:)');
%     
%     errorbar(tStep, ensMean, ensStd); hold on
%     
%     ii = trueSrc(iSrc);
%     plot(tStep,Xtrue(2*nSrc+(ii-1)*10+1:2*nSrc+(ii)*10),'o')
%     
%     ylim([-0.2 1.2])
%     title(sprintf('source %d',iSrc));
% %         [xp,xx] = ksdensity(Xe(i,:));
    % %     plot(xx,xp,'b','linewidth',1.5);hold on;
% %     [xp2,xx2] = ksdensity(Xe(i,:));
% %     plot(xx2,xp2,'r','linewidth',1.5,'linestyle','--');
% %     plot([Xtrue(i) Xtrue(i)],[0 max([max(xp),max(xp2)])*1.2],'color','k','linewidth',1.5);
% %     drawnow;
% % end
%     ylabel('Source intensity')
% end
% xlabel('time [day]')
% 
% % for iSrc = 1:nSrc
% % for i = 2*nSrc+1:5
% %     clf
% %     [xp,xx] = ksdensity(Xe(i,:));
% %     plot(xx,xp,'b','linewidth',1.5);hold on;
% %     [xp2,xx2] = ksdensity(Xe(i,:));
% %     plot(xx2,xp2,'r','linewidth',1.5,'linestyle','--');
% %     plot([Xtrue(i) Xtrue(i)],[0 max([max(xp),max(xp2)])*1.2],'color','k','linewidth',1.5);
% %     drawnow;
% % end
% % end
% legend('mean','true','location','southwest')
% 



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Ensemble Smoothers: Kalman(K) and Deep Learning (DL)

% X_DL_f = Xe; Y_DL_f = Ye; Y_DL_a = nan(size(Ye));
% X_K_f = Xe; Y_K_f = Ye; Y_K_a = nan(size(Ye));
% for iii = 1:Iter
%     
%     X_DL_a = ES_DL_update(X_DL_f,Y_DL_f,obs,sd*sqrt(Iter),range);
%     Xe = ES_K_update(X_K_f,Y_K_f,obs,sd*sqrt(Iter),range);
%     
%     parfor i = 1:Ne
%         Y_DL_a(i,:) = model_H(X_DL_a(i,:),i);
%     end
%     
%     parfor i = 1:Ne
%         Y_K_a(i,:) = model_H(Xe(i,:),i);
%     end
%     
%     X_K_f = Xe; Y_K_f = Y_K_a;
%     X_DL_f = X_DL_a; Y_DL_f = Y_DL_a;   
%   
% end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Results analysis

% figure
% for i = 1:5
%     subplot(2,3,i)
%     [xp,xx] = ksdensity(X_DL_a(:,i));
%     plot(xx,xp,'b','linewidth',1.5);hold on;
%     [xp2,xx2] = ksdensity(Xe(:,i));
%     plot(xx2,xp2,'r','linewidth',1.5,'linestyle','--');
%     plot([xreal(i) xreal(i)],[0 max([max(xp),max(xp2)])*1.2],'color','k','linewidth',1.5);
% end
% legend('ES_{(DL)}','ES_{(K)}','True values')

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Save results
% 
% save results
% % cd([currentdir,'\high_fidelity'])
% copyexample(Ne,-1);  % Delete the files for parallel computation
% % cd(currentdir)
% quit;
