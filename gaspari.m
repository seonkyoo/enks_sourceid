function [tau] = gaspari (do,range)
%----------------------------------------------------------------------------------
% SYNOPSIS:
%   [tau] = gaspari (do,range)
%
% DESCRIPTION:
%   Evaluate the Gaspari-Cohn correlation function with local support
%
% PARAMETERS:
%   do           -   distance (scalar or column)
%   range        -   correlation scale (scalar)
%
% RETURNS:
%   tau          -   localization factor (scalr or column)
%
%---------------------------------------------------------------------------------- 
  tau = zeros(size(do));
  ind1 = find(do < range);
  ind2 = find(do >= range & do < 2*range);
  ind0 = find(do >= 2*range);

  tau(ind1) = -0.25*(do(ind1)/range).^5 + 0.5*(do(ind1)/range).^4 ...
                +0.625*(do(ind1)/range).^3 -(5.0/3.0)*(do(ind1)/range).^2 + 1;

  tau(ind2) = (1.0/12.0)*(do(ind2)/range).^5 - 0.5*(do(ind2)/range).^4 ...
               +0.625*(do(ind2)/range).^3 + (5.0/3.0)*(do(ind2)/range).^2 ...
               -5*(do(ind2)/range) + 4 -(2.0/3.0)*(range./do(ind2));

  tau(ind0)=0;
  
return