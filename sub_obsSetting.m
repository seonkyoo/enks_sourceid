function [tObs, obsLoc, nOutC, nOutP] = ...
                      sub_obsSetting(M,obsType,nObsTime,nxObs,nyObs,nzObs)
input_gridStructure
%% -------------- observation time ---------------
% nObsTime = 10;
[~, ~, Uthrgh] = sub_boundaryCond(M);

tf = Lx / Uthrgh;                   % final time [sec]

tObs = linspace(tf/nObsTime,tf,nObsTime); % obs. time steps [sec]

%% -------------- observation location & dimension ---------------
% if ny == 1
%     nzObs = 4; nxObs = 8; nyObs = 1; 
% else
%     nzObs = 5; nxObs = 5; nyObs = 4; 
% end
zSpan = Lz/(nzObs+1); xSpan = Lx/(nxObs+1); ySpan = Ly/(nyObs+1);
zObs = linspace(zSpan/2,Lz-zSpan/2,nzObs);
xObs = linspace(xSpan/2,Lx-xSpan/2,nxObs);
yObs = linspace(ySpan/2,Ly-ySpan/2,nyObs);
if nyObs == 1; yObs = dy/2; end

[xx,zz,yy]=meshgrid(xObs,zObs,yObs); 

obsLoc = [xx(:) yy(:) zz(:)]; % observation locations (X,Z)
clear xx zz yy

nObsLoc = length(obsLoc);

% forward model output (Y) dimension
nOutC = nObsLoc*nObsTime;
nOutP = nObsLoc*nObsTime;
if M == 0; nOutP = nObsLoc; end
if size(obsType,2) == 1 && strcmp(obsType','prf')
    nOutC = 0;
elseif size(obsType,2) == 1 && strcmp(obsType','cnc')
    nOutP = 0;    
end

return