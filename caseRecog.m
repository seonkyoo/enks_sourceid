% =========================================================================
%               parallel pool creator 
% =========================================================================
%{
    nSrc: number of sources
    nSpces: number of tracer species. Should be 1 or nSrc. 
    nSrcInit: expected number of sources
    seed: seed number for randomization. For repeatable results.
    nCpu: cpu #. for parallel simulation
    Iter: iteration #. of ensemble smoothing
    Ne: ensemble #. 
%}
% clear; clc
caseDir = [pwd '/'];

ksourceID = strfind(caseDir, '/EnKS_sourceID');
% ksourceID = strfind(caseDir, '/Sphrc');

kiTrue= strfind(caseDir, '/itrue');

kkVar =  strfind(caseDir, '/kVar');

kM = strfind(caseDir,'/M');

knSrc = strfind(caseDir, '/nSrc');
knSrcInit = strfind(caseDir,'_nSrcInit');
kClstr = strfind(caseDir,'_clstr');

kNe = strfind(caseDir,'/Ne');
kprf = strfind(caseDir,'_prf');
kcnc = strfind(caseDir,'_cnc');

ksLocRange = strfind(caseDir, '/sLocRange');
ksCncRange = strfind(caseDir, '_sCncRange');
kkMapRange = strfind(caseDir, '_kMapRange');
kinflation= strfind(caseDir, '_inflation');

knObsTime = strfind(caseDir, '/nObsTime');
knxObs = strfind(caseDir, '_nxObs');
knyObs = strfind(caseDir, '_nyObs');
knzObs = strfind(caseDir, '_nzObs');

kIter = strfind(caseDir,'/Iter');
knCpu = strfind(caseDir,'_nCpu');
kjobId= strfind(caseDir,'_jobId');

%%
codeDir = caseDir(1:ksourceID+14);
kMatDir  = caseDir(1:kiTrue);
dataDir = caseDir(1:kIter);

%%
varioType = caseDir(ksourceID+15:kiTrue-1);
itrue = str2double(caseDir(kiTrue+6:kkVar-1));
kVar = str2double(caseDir(kkVar+5:kM-1));
M = str2double(caseDir(kM+2:knSrc-1));
nSrc = str2double(caseDir(knSrc+5:knSrcInit-1));
nSrcInit = str2double(caseDir(knSrcInit+9:kClstr-1));
doClstr = str2double(caseDir(kClstr+6:kNe-1));

clear 'ksourceID' 'kiTrue' 'kkVar' 'kM' 'knSrc' 'knSrcInit' 'kClstr'

%%
if ~isempty(kprf) && ~isempty(kcnc)
    obsType = ['cnc';'prf']'; 
    if kprf < kcnc
        Ne = str2double(caseDir(kNe+3:kprf-1));
        sd(1) = str2double(caseDir(kprf+4:kcnc-1));
        sd(2) = str2double(caseDir(kcnc+4:ksLocRange-1));
    else
        Ne = str2double(caseDir(kNe+3:kcnc-1));
        sd(1) = str2double(caseDir(kcnc+4:kprf-1));
        sd(2) = str2double(caseDir(kprf+4:ksLocRange-1));
    end
elseif ~isempty(kprf)
    obsType = ['prf';]'; 
    Ne = str2double(caseDir(kNe+3:kprf-1));
    sd = str2double(caseDir(kprf+4:ksLocRange-1));
elseif ~isempty(kcnc)
    obsType = ['cnc']'; 
    Ne = str2double(caseDir(kNe+3:kcnc-1));
    sd = str2double(caseDir(kcnc+4:ksLocRange-1));
end

sLocRange = str2double(caseDir(ksLocRange+10:ksCncRange-1));
sCncRange = str2double(caseDir(ksCncRange+10:kkMapRange-1));
kMapRange = str2double(caseDir(kkMapRange+10:kinflation-1));
inflation = str2double(caseDir(kinflation+10:knObsTime-1));

nObsTime = str2double(caseDir(knObsTime+9:knxObs-1));
nxObs = str2double(caseDir(knxObs+6:knyObs-1));
nyObs = str2double(caseDir(knyObs+6:knzObs-1));
nzObs = str2double(caseDir(knzObs+6:kIter-1));

Iter = str2double(caseDir(kIter+5:knCpu-1));
nCpu = str2double(caseDir(knCpu+5:kjobId-1));
jobId= str2double(caseDir(kjobId+6:end-1));

clear 'kNe' 'knSrcInit' 'kcnc' 'kprf' 
clear 'ksLocRange' 'ksCncRange' 'kkMapRange' 'kinflation'
clear 'knObsTime' 'knxObs' 'knyObs' 'knzObs' 'kIter' 'knCpu' 'kjobId'
