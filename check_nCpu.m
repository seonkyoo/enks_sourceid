flNameFail = sprintf([caseDir '/runFail']);
poolobj = gcp('nocreate'); % If no pool, do not create new one.
if isempty(poolobj)
    fprintf('fail to create parallel pool. \n');
    fidFail= fopen(flNameFail, 'a'); fclose(fidFail);    
    quit;
else
    poolsize = poolobj.NumWorkers;
    if poolsize ~= nCpu
        fprintf('%d/%d cores are created.\n',poolsize,nCpu);
        delete(poolobj);
        fidFail = fopen(flNameFail, 'a'); fclose(fidFail);    
        quit;
    end
    fprintf('%d cores are successfully created. \n',poolsize);
end    