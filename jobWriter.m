clear all; clc; 
%% ========================================================================
% 
%                              jobWriter.m 
% 
% -------------------------------------------------------------------------
% Conditions:
%     nSrc = 3;
%     nSrcInit = [3:10];
%     seed = 1;
%     nCpu = 32;
%     Iter = 10;
%     Ne = 500;
% Jobs:
     queueName = ['small']; cmdFlName = sprintf('main');
%     queueName = ['amdsmall,small']; cmdFlName = sprintf('main');
%    queueName = ['pkkang']; cmdFlName = sprintf('main');
% 
% ========================================================================

clc
codeDir = '~/sourceID/EnKS_sourceID/';
fidSource = fopen([codeDir sprintf('bash_%s',cmdFlName)],'w');

nSrc = 3;
Ne = 500;

nNodes = 1;

wallTime = '96:00:00';
% np = 1; mem =  ['1gb'];
np = 24; mem = ['1900mb'];

Iter = 10;
nCpu = nNodes * np;

varioType = ['Sphrc'; 'Expnt'; 'Gausn'; 'Sph3d']';
% Sphrc =  [527 537 563 583 603 709 721 727]

sdCnc = 0.02; sdPrf = 10;
% txtObs=sprintf(['cnc%.3f'], sdCnc); dataScenario = 'C';
% txtObs=sprintf(['prf%d'], sdPrf); dataScenario = 'P';
txtObs=sprintf(['cnc%.3f_prf%d'], sdCnc,sdPrf); dataScenario = 'J';

nObsTime = 10; nzObs = 5; nxObs = 5; nyObs = 4; 
% nObsTime = 10; nzObs = 5; nxObs = 10; nyObs = 5; 

nCase = 0;
for sLocRange =25% [0 25]
for sCncRange = sLocRange
for kMapRange = sLocRange
for inflation = 0.0%[0 0.2]
for iVario = 4%:3
iSeed = -1;
for seed = [501:523]%[502 527 537 563 583 603 709 721 727 851]%503 504 ]%727 851 ] %max 2*4*4*27 = 864
iSeed = iSeed + 1;
ikVar = -1;
for kVar = [0.25 0.5 1 2 3]%[0.25 0.5 1 2 3]%[0.25 0.5 1 2 3]
ikVar = ikVar + 1;
for nSrcInit = 3%:10
for clstr =1% [0 1 ] % 0: no clustering | 1: only once | 2: continuously 
for M = [10 1 0.1]% 0]%[0 0.01 0.1  1 5 10]%
    dataDir = [codeDir ...
        sprintf(['%s/itrue%d/kVar%.2f/M%.3f/nSrc%d_nSrcInit%d_clstr%d/'...
                 'Ne%d_%s/sLocRange%d_sCncRange%d_kMapRange%d_inflation%.2f/'...
                 'nObsTime%d_nxObs%d_nyObs%d_nzObs%d/'],...
                  varioType(:,iVario)',seed,kVar,M,nSrc,nSrcInit,clstr,...
                  Ne,txtObs,sLocRange,sCncRange,kMapRange,inflation,...
                  nObsTime,nxObs,nyObs,nzObs)];
    if ~exist([dataDir sprintf('update_Iter%d.mat',0)],'file')
        nFolder = 0;
    else
        switch M
            case 10
                nFolder = 0;%1;%ikVar;
%                 if kVar == 3; nFolder = nFolder+1; end
            case 1
                nFolder = 0;%ikVar;
            case 0.1
                nFolder = 0;
            case 0
                nFolder = 0;
        end
%         if kVar == 2 || kVar == 3; nFolder = nFolder+1; end
    end
for iFolder =  [0:nFolder]

    caseDir = [dataDir sprintf(['Iter%d_nCpu%d_jobId%d/'],Iter,nCpu,iFolder)];


    if ~exist(caseDir,'dir'); mkdir(caseDir); end

    %jobName = sprintf('M%.1f%s%d',M(iM),dataScenario,jobId);
%     jobName = sprintf('I%.1f%s%d',inflation,dataScenario,jobId);
    %jobName = sprintf('M%.1f_%d',M,jobId);
%     jobName = sprintf('%d_%d_%d',seed-500,round(M),jobId);
    jobName = sprintf('%d_%d_%d_%d',iSeed,ikVar,round(M),iFolder);


    doRun = false; 
    existFail = false; existLog = false; existOut = false; 
    tOut = nan; tLog = nan;
    
    if ~exist([dataDir sprintf('update_Iter%d.mat',Iter)],'file')
        if exist([caseDir 'runFail'],'file'); existFail = true; end

        if exist([caseDir cmdFlName '.log'],'file')
            existLog = true;
            logFlInfo = dir([caseDir cmdFlName '.log']);
            tLog = logFlInfo.datenum;
        end

        caseDirFl = dir(caseDir);
        for il = 3:numel(caseDirFl)
            if strcmp(caseDirFl(il).name(end-2:end),'out')
                existOut = true;
                outFlName = caseDirFl(il).name;
                tOut = caseDirFl(il).datenum;
                outMesg = caseDirFl(il).bytes;
                jobId = caseDirFl(il).name(7:end-4);
            end
            
        end
        
        
        formatIn = 'dd-mmm-yyyy HH:MM:SS';
%         DateString = '12-Apr-2021 13:35:00';
%         tTrshld = datenum(DateString,formatIn);
        tTrshld = now-1/24*30/60; % 15 min earlier than now
%         datetime(tTrshld ,'convertFrom','datenum')

        if ~existOut 
            doRun = true; 
        elseif outMesg > 0
            doRun = true; 
        end

        if existFail; doRun = true; end
        if existLog
            if tLog < tTrshld 
                doRun = true;
            end
        end
        
        %doRun = false;
        %if ~exist([dataDir sprintf('update_Iter%d.mat',10)],'file')
        %    doRun = true;
        %end

    end
    
    if doRun% && ikVar >= 4 && M == 10
        if existOut; fprintf(fidSource, 'scancel %s \n',jobId); end
        if existLog; fprintf(fidSource, 'rm %s%s.log \n',caseDir,cmdFlName ); end
        if existFail; fprintf(fidSource, 'rm %srunFail \n',caseDir); end
        if existOut; fprintf(fidSource, 'rm %s%s\n',caseDir,outFlName); end
        for il = 3:numel(caseDirFl)
            if strcmp(caseDirFl(il).name(end-2:end),'occ')
                occFlName = caseDirFl(il).name;
                fprintf(fidSource, 'rm %s%s\n',dataDir,occFlName); 
                fprintf(fidSource, 'rm %s%s\n',caseDir,occFlName); 
            end
        end

        nCase = nCase + 1;
        fprintf(fidSource, 'cd %s \n',caseDir);
        fprintf(fidSource, 'cp %s%s.cmd . \n',codeDir,cmdFlName);
        fprintf(fidSource, 'sbatch -p %s -J %s %s.cmd \n\n',queueName,jobName,cmdFlName);

        %nCase = nCase + 1;
        %fprintf(fidSource, '%s\n',caseDir);
        %if existOut; fprintf(fidSource, 'cat %s%s\n',caseDir,outFlName); end
        
        %fprintf(fidSource, '%s \n',caseDir); nCase = nCase + 1;

%         fprintf(fidSource, 'matlab -nodisplay <%s%s.m  \n',codeDir,cmdFlName);
    end
end
end
end
end
end
end
end
end
end
end
end

fprintf(fidSource, 'cd %s\n',codeDir);
fclose(fidSource);

fprintf('nCase = %d\n', nCase);


%% cmd file ================================================================


fidSource = fopen([codeDir sprintf('%s.cmd',cmdFlName)],'w');

fprintf(fidSource, '#!/bin/bash -l \n');
fprintf(fidSource, '#SBATCH --time=%s \n',wallTime);
fprintf(fidSource, '#SBATCH --nodes=%d \n',nNodes);
fprintf(fidSource, '#SBATCH --ntasks-per-node=%d \n',np);
fprintf(fidSource, '#SBATCH --mem-per-cpu=%s \n\n',mem);

fprintf(fidSource, 'module load matlab \n');
fprintf(fidSource, 'matlab -nodisplay <%s%s.m> %s.log \n',...
                                    codeDir,cmdFlName,cmdFlName);


fclose(fidSource);

