%% physical constants
D = 1;              % dispersivity [m]

visco = 10^-3;            % viscosity [kg/m/s]
g = 9.8; %9.8;         % gravitational acceleration [m/s^2]. 
                       % 0: horizontal | 9.8: vertical
rho0 = 1000;           % fresh water density [kg/m^3]
DelRho = 1.28;         % TCE solubility into water [kg/m^3]
rho1 = rho0+DelRho;    % saline water density [kg/m^3]
phi = 0.3;             % porosity

kTrueMean = exp(-23);
